#include <protoipc/router.hh>

#include <iostream>
#include <map>
#include <optional>
#include <thread>

#include "database.sidl.hh"

namespace db
{

class DatabaseReceiverImpl : public DatabaseReceiver
{
  public:
    DatabaseReceiverImpl(std::string version) { data_["version"] = version; }

    bool add(std::string key, std::string value) override
    {
        data_[key] = value;
        return true;
    }

    bool get(std::string key, std::optional<std::string> *result) override
    {
        auto it = data_.find(key);

        if (it == data_.end())
            *result = std::nullopt;
        else
            *result = it->second;

        return true;
    }

  private:
    std::map<std::string, std::string> data_;
};

} // namespace db

int main(int argc, char **argv)
{
    auto router = ipc::Router::make();

    auto [client_chan_id, client_router_port] = router->create_port();
    auto [receiver_chan_id, receiver_router_port] = router->create_port();

    // Setting up channels
    rpc::Channel client_chan(client_chan_id, std::move(client_router_port));
    rpc::Channel receiver_chan(receiver_chan_id, std::move(receiver_router_port));

    auto receiver_id = receiver_chan.bind<db::DatabaseReceiverImpl>("0.0.1-beta");
    auto proxy = client_chan.connect<db::DatabaseProxy>(receiver_chan_id, receiver_id);

    std::thread router_thread([&]() { router->loop(); });

    std::thread client_chan_thread([&]() { client_chan.loop(); });

    std::thread receiver_chan_thread([&]() { receiver_chan.loop(); });

    // Using the proxy
    std::optional<std::string> value;

    if (!proxy->get("version", &value))
    {
        std::cout << "proxy->get(...) failed\n";
        return 1;
    }

    if (!value)
    {
        std::cout << "proxy->get(\"version\") should not be empty\n";
        return 1;
    }

    std::cout << "Version: " << *value << '\n';
    receiver_chan.stop();
    client_chan.stop();
    receiver_chan_thread.join();
    client_chan_thread.join();
    router->terminate();
    router_thread.join();

    return 0;
}
