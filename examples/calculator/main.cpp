#include <protoipc/router.hh>

#include <iostream>
#include <thread>

#include "calculator.sidl.hh"

// Implementing the receiver methods
namespace math
{
class CalculatorImpl : public CalculatorReceiver
{
  public:
    bool add(std::int64_t lhs, std::int64_t rhs, std::int64_t *result) override
    {
        *result = lhs + rhs;
        return true;
    }

    bool sub(std::int64_t lhs, std::int64_t rhs, std::int64_t *result) override
    {
        *result = lhs - rhs;
        return true;
    }

    bool mul(std::int64_t lhs, std::int64_t rhs, std::int64_t *result) override
    {
        *result = lhs * rhs;
        return true;
    }

    bool div(std::int64_t lhs, std::int64_t rhs, std::int64_t *result) override
    {
        *result = lhs / rhs;
        return true;
    }
};
} // namespace math

int main(int argc, char **argv)
{
    auto router = ipc::Router::make();

    auto [client_chan_id, client_router_port] = router->create_port();
    auto [receiver_chan_id, receiver_router_port] = router->create_port();

    // Setting up channels
    rpc::Channel client_chan(client_chan_id, std::move(client_router_port));
    rpc::Channel receiver_chan(receiver_chan_id, std::move(receiver_router_port));

    rpc::ObjectId receiver_id = receiver_chan.bind<math::CalculatorImpl>();
    auto proxy = client_chan.connect<math::CalculatorProxy>(receiver_chan_id, receiver_id);

    std::thread router_thread([&]() { router->loop(); });

    std::thread client_chan_thread([&]() { client_chan.loop(); });

    std::thread receiver_chan_thread([&]() { receiver_chan.loop(); });

    // Now doing computations
    std::int64_t lhs = 2;
    std::int64_t rhs = 40;
    std::int64_t result = 0;

    if (!proxy->add(lhs, rhs, &result))
    {
        std::cout << "proxy->add(...) failed\n";
        return 1;
    }

    std::cout << "proxy->add(...) = " << result << '\n';
    receiver_chan.stop();
    client_chan.stop();
    receiver_chan_thread.join();
    client_chan_thread.join();
    router->terminate();
    router_thread.join();

    return 0;
}
