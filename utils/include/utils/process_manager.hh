#ifndef UTILS_PROCESS_MANAGER_HH
#define UTILS_PROCESS_MANAGER_HH

#include <protoipc/logger.hh>

#include <cassert>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#if defined(__linux)
#include <cstring>
#include <sys/wait.h>
#include <unistd.h>

class ProcessManager
{
  public:
    ProcessManager(const std::vector<std::string> &args)
    {
        char **c_args;
        size_t nb_args = 0;
        c_args = (char **)std::malloc(sizeof(char *) * (args.size() + 1));
        for (const auto &arg : args)
        {
            c_args[nb_args] = strdup(arg.c_str());
            nb_args++;
        }
        c_args[nb_args] = NULL;

        pid_t pid = fork();
        if (pid == -1)
            throw std::runtime_error("Unable to spawn new process");
        if (pid == 0)
            execv(c_args[0], c_args);
        pid_ = pid;

        for (size_t i = 0; i < nb_args; i++)
        {
            free(c_args[i]);
        }
        free(c_args);
    }

    ~ProcessManager()
    {
        if (pid_ != 0)
            waitpid(pid_, NULL, 0);
    }

    void wait()
    {
        ipc::Logger::process_instance().debug() << "[PROCESS] start waiting";
        assert(pid_ != 0);
        waitpid(pid_, NULL, 0);
        pid_ = 0;
    }

  private:
    pid_t pid_;
};

#elif defined(_WIN32)
#include <windows.h>

class ProcessManager
{
  public:
    ProcessManager(const std::vector<std::string> &args)
    {
        char *c_cmdline;
        si = {};
        si.cb = sizeof(si);
        pi = {};

        std::string cmdline;
        for (const auto &arg : args)
        {
            cmdline += arg + " ";
        }
        c_cmdline = strdup(cmdline.c_str());
        std::printf("[PROCESS] Running new process with command: %s\n", c_cmdline);

        if (!CreateProcessA(NULL, c_cmdline, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi))
            throw std::runtime_error("Process Creation failed");
        free(c_cmdline);
    }

    ~ProcessManager()
    {
        WaitForSingleObject(pi.hProcess, INFINITE);

        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }

  private:
    STARTUPINFOA si;
    PROCESS_INFORMATION pi;
};
#endif
#endif
