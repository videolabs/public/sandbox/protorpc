# ProtoRPC

[![pipeline status](https://gitlab.com/videolabs/public/sandbox/protorpc/badges/master/pipeline.svg)](https://gitlab.com/videolabs/public/sandbox/protorpc/-/commits/master)
[![coverage report](https://gitlab.com/videolabs/public/sandbox/protorpc/badges/master/coverage.svg)](https://gitlab.com/videolabs/public/sandbox/protorpc/-/commits/master)
[![Latest Release](https://gitlab.com/videolabs/public/sandbox/protorpc/-/badges/release.svg)](https://gitlab.com/videolabs/public/sandbox/protorpc/-/releases)

This repository contains the **prorotype** of an Inter Process Communication
library for the [Hardened VLC media
player](https://gitlab.com/videolabs/public/sandbox/vlc).  
A full walkthrough of the design choices of the prototype and a further
description of the sandboxing mechanisms can be found in our global user
[documentation](https://gitlab.com/videolabs/public/sandbox/documentation/-/wikis/home)
on the matter.

## Platforms

Protorpc is available for the following platforms:

- GNU/Linux
- Windows (from 10 and later) is a work in progress (see !28)

## Build

```bash-session
meson setup Build
ninja -C build
```

Examples can be built with the following `meson setup` option:
```bash-session
meson setup build -Dbuild_examples=true
```

## Overview

The repository contains two different API and one `IDL` generator:

### ProtoIPC

`libprotoipc` provides a light cross-platform abstraction over OS specific IPC
mechanisms.

The first exposed object by the API is `ipc::Port`. It is an abstraction over
an IPC endpoint. Ports can send and receive `ipc::Message` objects on the wire.
These objects contain a destination field (destination port id), the payload
bytes and handles if any.

The second exposed object is `ipc::Router`. The router's job, is to dispatch
messages between different ports. It is the recommended method whenever more
than two processes need to be interconnected.

### ProtoRPC

`libprotorpc` provides abstraction to  call remote objects and receive back
their response. It defines the calling object as a `rpc::RpcProxy` and the
receiving handler object as a `rpc::RpcReceiver`.  

The RPC objects communicate with each other through a `rpc::Channel` to which
they are bound. The messages they send are routed by the channel through the
`ipc::Port` it contains.  
Each object on the *"RPC network"* has an address, the object ID, which is
local to the current channel and the port ID which is the identifier used by
the IPC layer to route the message to the correct channel port.  
The library is designed to work with the following configuration:

![protorpc design](extras/assets/rpc_library_design.avif)

In this design the channels and the router could very well be in the same
process or a totally different one.

Now that all the components are in place, let's see how to create objects. The
receiver object is the one handling the logic and creating one is simple as you
just need to create a class inheriting from `rpc::RpcReceiver` and implement
the `on_message` function which has the following prototype:

```c++
virtual void on_message(Channel& chan, ObjectId current_object, PortId source_port, rpc::Message& message) = 0;
```

- `chan` is the receiving channel
- `current_object` is the receiver's object id
- `source_port` the port on which the calling object listens
- `message` represents the message itself

`rpc::Message` is the RPC equivalent of `ipc::Message`. It contains information
to be handled by THE rpc layer (source object, destination object, request
opcode). In the `on_message` function you only need to parse the bytes of the
request, handle it, serialize the result and send it back using
`Channel::send_message`.

The proxy part is more freeform. As for the receiver, you need to create a
class inheriting `rpc::RpcProxy`. Then you can implement the methods as you see
fit. Each method must have its arguments serialized and should send them
through the proxy's channel using `Channel::send_request`. It should then
unserialize the result and return it. The convention used for the methods is to
return a `bool` (`true` for success), take the arguments as copy and results as
pointer arguments. As such, an addition method could be defined like this:

```c++
class AdderProxy : public rpc::RpcProxy
{
    //...
    bool add(int lhs, int rhs, int* result);
    //...
};
```

`rpc::Channel` then provides helper functions to register objects:

```c++
// Proxy objects
// The connect method returns a shared pointer to T where T is a class inheriting
// from rpc::RpcProxy
auto proxy = channel.connect<T>(remote_port_id, remote_object_id);

// Call to remote object
proxy->remote_method_1(...);

// Receiver objects
// Where T is a class inheriting from rpc::RpcReceiver
// This function returns an id because the receiver is to be called by the channel directly
// and is not to be directly manipulated.
rpc::ObjectId receiver_id = channel.bind<T>(receiver contructor args...);

// Binding a receiver to a specific object id. This is especially useful for bootstrapping.
channel.bind_static<T>(receiver constructor args...);
```

This approach works fine but a lot of boilerplate and error prone code
needs to be written. This is why an `IDL` and its compiler were created.

### SIDL and serialization

SIDL (Simple `IDL`) is the `IDL` language created and used by this project. This
language supports a small set of constructs:

```text
# namespaces
namespace test
{
    # user defined type
    struct User
    {
        string name;
        u32 uid;
    }

    struct Database
    {
        # containers
        vec<User> users;
    }

    # Interface definition (will generate proxy object and receiver interface)
    interface DatabaseDriver
    {
        # "Asynchronous" method (not waiting for completion acknowledgement
        # by the remote)
        print_users();

        # method withtout return value but waiting for completion acknownledgement
        # by the remote
        print_users_sync() -> ();

        # Method returning a value
        add_user(string name, u32 uid) -> (bool success);

        # Use user defined types
        add_user_object(User user) -> (bool success);

        # Method returning multiple values
        get_random_pair() -> (User a, User b);
    }
}
```

The types of `SIDL` match the native `C++` types (eg. `u64` -> `std::uint64_t`).

The current implementation of the compiler is called `sidlcc` and is written in
`python`. It is small and modular enough that another backend than `C++` could
easily be implemented.  
`sidlcc` takes as input a `.sidl` source file and generates a header file
(`.hh`) as well as an implementation file (`.cpp`). The header file contains
all user defined objects, the serialization code as well as the class
definition for both proxy and receiver objects.  
In the previous example for the `DatabaseDriver` interface a
`DatabaseDriverProxy` class inheriting `rpc::RpcProxy` and a
`DataBaseDriverReceiver` class inheriting `rpc::RpcReceiver` would be
generated. The `C++` implementation file contains the generated `on_message`
method for the receiver object as well as all the generated method calls for
the proxy. Without any modifications the proxy object can be connected to a
remote object. For the receiver, a class must be created inheriting from the
generated class and implementing the pure virtual handler methods.

As for the serialization, it's done by templates in the current implementation.
It can be seen in `libprotorpc/include/serializer.hh` and
`protorpc/include/unserializer.hh`. All `SIDL` native types are supported, for
custom types both `rpc::serializable` and `rpc::unserializable` traits must be
implemented (which is what `sidlcc` does under the hood for user defined
types).
