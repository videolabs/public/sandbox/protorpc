#include <protoipc/linux/router.hh>

#include <stdexcept>
#include <string.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/socket.h>
#include <unistd.h>

namespace ipc
{

LinuxRouter::LinuxRouter()
{
    epoll_fd_ = epoll_create1(0);

    if (epoll_fd_ == -1)
        throw std::runtime_error("Could not create epoll fd");

    event_fd_ = eventfd(0, EFD_CLOEXEC);
    struct epoll_event ev = {EPOLLIN, {nullptr}};
    if (epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, event_fd_, &ev) == -1)
    {
        std::perror("EPOLL_CTL_ADD");
        throw std::runtime_error("epoll");
    }
}

LinuxRouter::~LinuxRouter()
{
    close(epoll_fd_);
    close(event_fd_);
}

void LinuxRouter::terminate()
{
    end_ = true;
    const uint64_t val = 1;
    write(event_fd_, &val, sizeof(uint64_t));
}

PortId LinuxRouter::add_port(int fd)
{
    struct epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.u64 = current_id_;

    if (epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, fd, &ev) == -1)
    {
        std::perror("EPOLL_CTL_ADD");
        throw std::runtime_error("epoll");
    }

    {
        std::lock_guard<std::mutex> l(lock_);
        ports_[current_id_] = std::make_unique<LinuxPort>(Handle{fd});
    }

    return current_id_++;
}

bool LinuxRouter::remove_port(PortId id)
{
    std::lock_guard<std::mutex> l(lock_);
    auto it = ports_.find(id);

    if (it == ports_.end())
        return false;

    ports_.erase(it);

    return true;
}

ipc::PortError LinuxRouter::receive(Message &message)
{
    struct epoll_event ev;

    int res = 0;

    while ((res = epoll_wait(epoll_fd_, &ev, 1, -1)) == -1)
    {
        if (errno == EINTR)
            continue;
        else
            return ipc::PortError::PollError;
    }

    if (end_)
        return ipc::PortError::Ok;

    if (ev.events & EPOLLHUP)
        return ipc::PortError::Ok;

    std::lock_guard<std::mutex> l(lock_);
    const PortId port_id = ev.data.u64;
    auto source = ports_.find(port_id);

    if (source == ports_.end())
        return ipc::PortError::BadFileDescriptor;

    ipc::PortError err = source->second->receive(message);
    if (err != ipc::PortError::Ok)
        return err;

    return PortError::Ok;
}

ipc::PortError LinuxRouter::retransmit(Message &&message)
{
    std::lock_guard<std::mutex> l(lock_);
    auto destination = ports_.find(message.destination);

    if (destination == ports_.end())
        return ipc::PortError::BadFileDescriptor;

    ipc::PortError err = destination->second->send(message);
    if (err != ipc::PortError::Ok)
        return err;

    return PortError::Ok;
}

ipc::PortError LinuxRouter::loop()
{
    for (;;)
    {
        constexpr int EPOLL_MAX_EVENTS = 16;
        struct epoll_event events[EPOLL_MAX_EVENTS];

        int res = 0;

        while ((res = epoll_wait(epoll_fd_, events, EPOLL_MAX_EVENTS, -1)) == -1)
        {
            if (errno == EINTR)
                continue;
            else
                return ipc::PortError::PollError;
        }

        if (end_)
            return ipc::PortError::Ok;

        for (int i = 0; i < res; i++)
        {
            struct epoll_event ev = events[i];
            if (ev.events & EPOLLHUP)
                return ipc::PortError::Ok;

            std::lock_guard<std::mutex> l(lock_);
            const PortId port_id = ev.data.u64;
            auto source = ports_.find(port_id);

            if (source == ports_.end())
                return ipc::PortError::BadFileDescriptor;

            ipc::Message message;
            ipc::PortError err = source->second->receive(message);

            if (err != ipc::PortError::Ok)
                return err;

            auto destination = ports_.find(message.destination);

            if (destination == ports_.end())
                return ipc::PortError::BadFileDescriptor;

            err = destination->second->send(message);

            if (err != ipc::PortError::Ok)
                return err;
        }
    }
}

std::pair<PortId, std::unique_ptr<Port>> LinuxRouter::create_port()
{
    int pair[2];

    if (socketpair(AF_UNIX, SOCK_STREAM, 0, pair) == -1)
        throw std::runtime_error("couldn't create socketpair" + std::string(strerror(errno)));

    PortId id = add_port(pair[0]);
    std::unique_ptr<Port> from =
        std::make_unique<Port>(std::make_unique<ipc::LinuxPort>(Handle{pair[1]}));
    close(pair[0]);
    close(pair[1]);
    return {id, std::move(from)};
}

std::unique_ptr<Router> Router::make()
{
    auto backend = std::make_unique<LinuxRouter>();
    return std::make_unique<Router>(std::move(backend));
}

} // namespace ipc
