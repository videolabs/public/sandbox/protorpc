#include <protoipc/linux/port.hh>

#include <cerrno>
#include <cstring>
#include <fcntl.h>
#include <mutex>
#include <poll.h>
#include <stdexcept>
#include <sys/eventfd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// XXX: High enough limit for common cases (same as kMaxSendmsgHandles in mojo)
constexpr std::size_t IPC_MAX_HANDLES = 128;

namespace ipc
{

LinuxPort::LinuxPort() : pipe_fd_(-1), event_fd_(-1) {}

LinuxPort::LinuxPort(Handle fd)
{
    pipe_fd_ = fcntl(fd.handle, F_DUPFD_CLOEXEC, 0);
    if (pipe_fd_ == -1)
        throw std::runtime_error("Couldn't duplicate fd at creation\n: " +
                                 std::string(std::strerror(errno)));
    event_fd_ = eventfd(0, EFD_CLOEXEC);
}

LinuxPort::~LinuxPort()
{
    if (event_fd_ != -1)
        ::close(event_fd_);
    if (pipe_fd_ != -1)
        ::close(pipe_fd_);
}

/**
 * Sends a message over a native port.
 *
 * The message is sent over two iovecs. The first iovec contains the ipc header
 * composed of [payload_size, handle_count, destination]. The second iovec
 * contains the actual payload.
 */
PortError LinuxPort::send(const Message &message)
{
    struct pollfd pfds[2];
    pfds[0] = {.fd = pipe_fd_, .events = POLLOUT};
    pfds[1] = {.fd = event_fd_, .events = POLLIN};

    char sendmsg_control[CMSG_SPACE(sizeof(int) * IPC_MAX_HANDLES * 2)] = {0};
    std::uint64_t ipc_header[] = {
        (uint64_t)message.kind,
        message.payload.size(),
        message.handles.size(),
        message.source,
        message.destination,
    };

    if (message.handles.size() > IPC_MAX_HANDLES)
        return PortError::TooManyHandles;

    struct msghdr header = {};
    struct iovec iov[2];

    // Header iovec
    iov[0].iov_base = ipc_header;
    iov[0].iov_len = sizeof(ipc_header);

    // Data iovec
    iov[1].iov_base = const_cast<std::uint8_t *>(message.payload.data());
    iov[1].iov_len = message.payload.size();

    header.msg_iov = iov;
    header.msg_iovlen = 2;

    if (message.handles.size() > 0)
    {
        header.msg_control = sendmsg_control;
        header.msg_controllen = CMSG_SPACE(sizeof(int) * message.handles.size());

        struct cmsghdr *cmsg = CMSG_FIRSTHDR(&header);
        cmsg->cmsg_level = SOL_SOCKET;
        cmsg->cmsg_type = SCM_RIGHTS;
        cmsg->cmsg_len = CMSG_LEN(sizeof(int) * message.handles.size());

        std::memcpy(CMSG_DATA(cmsg), message.handles.data(), sizeof(int) * message.handles.size());
    }

    for (;;)
    {
        int pollrc = poll(pfds, 2, -1);
        if (pollrc == -1)
        {
            switch (errno)
            {
                case EINTR:
                    continue;
                case EBADF:
                    return PortError::BadFileDescriptor;
                default:
                    return PortError::Unknown;
            }
        }

        if (pfds[0].revents & POLLERR)
            return PortError::ConnectionClosed;
        if (pfds[0].revents & POLLHUP)
            return PortError::ConnectionClosed;
        if (pfds[1].revents & POLLIN)
            return PortError::ConnectionClosed;
        if (pfds[0].revents & POLLOUT)
            break;
    }

    int err = 0;
    while ((err = sendmsg(pipe_fd_, &header, 0)) == -1)
    {
        switch (errno)
        {
            case EINTR:
                continue;
            case EBADF:
                return PortError::BadFileDescriptor;
            case EPIPE:
                return PortError::ConnectionClosed;
            default:
                return PortError::Unknown;
        }
    }

    return PortError::Ok;
}

/**
 * Receives a message from a native port.
 */
PortError LinuxPort::receive(Message &message)
{
    struct pollfd pfds[2];
    pfds[0] = {.fd = pipe_fd_, .events = POLLIN};
    pfds[1] = {.fd = event_fd_, .events = POLLIN};

    char recvmsg_control[CMSG_SPACE(sizeof(int) * IPC_MAX_HANDLES * 2)];
    std::uint64_t ipc_header[5];

    struct msghdr header = {};
    struct iovec iov[2];

    // Header iovec
    iov[0].iov_base = ipc_header;
    iov[0].iov_len = sizeof(ipc_header);

    // Data iovec (temporary)
    iov[1].iov_base = NULL;
    iov[1].iov_len = 0;

    header.msg_iov = iov;
    header.msg_iovlen = 2;

    int l_recv = 0;

    for (;;)
    {
        int pollrc = poll(pfds, 2, -1);
        if (pollrc == -1)
        {
            switch (errno)
            {
                case EINTR:
                    continue;
                case EBADF:
                    return PortError::BadFileDescriptor;
                default:
                    return PortError::Unknown;
            }
        }

        if (pfds[0].revents & POLLERR)
            return PortError::ConnectionClosed;
        if (pfds[0].revents & POLLHUP)
            return PortError::ConnectionClosed;
        if (pfds[1].revents & POLLIN)
            return PortError::ConnectionClosed;
        if (pfds[0].revents & POLLIN)
            break;
    }

    l_recv = recvmsg(pipe_fd_, &header, MSG_PEEK | MSG_DONTWAIT);
    if (l_recv == 0)
        return PortError::ConnectionClosed;

    header.msg_control = recvmsg_control;
    header.msg_controllen = sizeof(recvmsg_control);

    if (ipc_header[2] > IPC_MAX_HANDLES)
        return PortError::TooManyHandles;

    // TODO: check message.kind possible value, maybe using a function
    //       from Messagekind and raising exception?
    message.kind = (MessageKind)ipc_header[0];
    message.payload.resize(ipc_header[1]);
    message.handles.resize(ipc_header[2]);
    message.source = ipc_header[3];
    message.destination = ipc_header[4];

    iov[1].iov_base = message.payload.data();
    iov[1].iov_len = message.payload.size();

    while ((l_recv = recvmsg(pipe_fd_, &header, MSG_WAITALL)) == -1)
    {
        switch (errno)
        {
            case EINTR:
                continue;
            case EBADF:
                return PortError::BadFileDescriptor;
            default:
                return PortError::Unknown;
        }
    }

    if (l_recv == 0)
        return PortError::ConnectionClosed;

    if (message.handles.size() > 0)
    {
        struct cmsghdr *cmsg = CMSG_FIRSTHDR(&header);

        if (!cmsg)
            return PortError::IncompleteMessage;

        std::memcpy(message.handles.data(), CMSG_DATA(cmsg), sizeof(int) * message.handles.size());
    }

    return PortError::Ok;
}

void LinuxPort::close()
{
    notify();
    if (pipe_fd_ != -1)
    {
        ::close(pipe_fd_);
        pipe_fd_ = -1;
    }

    if (event_fd_ != -1)
    {
        ::close(event_fd_);
        event_fd_ = -1;
    }
}

int LinuxPort::notify()
{
    const uint64_t val = 1;
    return write(event_fd_, &val, sizeof(uint64_t));
}

Handle LinuxPort::dup() { return Handle{::dup(pipe_fd_)}; }

std::unique_ptr<Port> Port::from_handle(Handle handle)
{
    return std::make_unique<Port>(std::make_unique<LinuxPort>(handle));
}

} // namespace ipc
