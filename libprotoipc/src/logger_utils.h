#ifndef IPC_LOGGER_UTILS_H
#define IPC_LOGGER_UTILS_H

#include <protoipc/logger.hh>

#include <cstddef>

namespace ipc
{

class TracingSession
{
  public:
    TracingSession(std::string file, std::size_t line, std::string func, LoggingSession &&session)
        : session_{std::move(session)}
    {
        session_.append(file + ":" + std::to_string(line) + "(" + func + "): ");
    }

    template <typename T> TracingSession &operator<<(const T &value)
    {
        session_ << value;
        return *this;
    }

  private:
    LoggingSession session_;
};

} // namespace ipc

#define LOG()                                                                                      \
    ::ipc::TracingSession                                                                          \
    {                                                                                              \
        __FILE__, __LINE__, __func__, ::ipc::Logger::process_instance().debug()                    \
    }

#endif
