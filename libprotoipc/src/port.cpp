#include <protoipc/logger.hh>
#include <protoipc/port.hh>

#include <cassert>

#include "logger_utils.h"

namespace ipc
{

Port::Port(std::unique_ptr<PortBackend> impl) : impl_(std::move(impl)) {}

PortError Port::send(const Message &message)
{
    std::unique_lock<std::mutex> lock{lock_};

    /* Acknowledgement message are special in the IPC protocol
     * since they don't need to be acknowledged back. We should
     * send them directly. */
    if (message.kind == MessageKind::ACKNOWLEDGE)
    {
        LOG() << "sending --> ACK: " << message.desc();
        return impl_->send(message);
    }

    /* If we're waiting for an acknowledgement message, we cannot just
     * wait and we might need to receive message also. */
    while (pending_send_)
    {
        if (pending_receiving_)
        {
            cond_.wait(lock, [this] { return !pending_receiving_ || !pending_send_; });
            continue;
        }

        if (available_message_ && message_.kind == MessageKind::ACKNOWLEDGE)
        {
            available_message_ = false;
            pending_send_ = false;
            break;
        }

        pending_receiving_ = true;
        lock.unlock();

        Message msg = {};
        PortError error = impl_->receive(msg);
        if (error != PortError::Ok)
            return error;

        lock.lock();
        pending_receiving_ = false;

        if (msg.kind == MessageKind::ACKNOWLEDGE)
        {
            LOG() << "received <-- ACK: " << msg.desc();
            pending_send_ = false;
            break;
        }

        if (available_message_)
        {
            LOG() << "Dropping message received before acknowledgement: " << msg.desc();
        }
        else
        {
            LOG() << "Storing message: " << msg.desc();

            message_ = std::move(msg);
            available_message_ = true;
            cond_.notify_all();
        }
    }

    LOG() << "sending --> PAYLOAD: " << message.desc();

    PortError error = impl_->send(message);
    if (error != PortError::Ok)
        return error;

    pending_send_ = true;
    return PortError::Ok;
}

PortError Port::receive_ack()
{
    std::unique_lock<std::mutex> lock{lock_};
    assert(pending_send_);

    /* If we're waiting for an acknowledgement message, we cannot just
     * wait and we might need to receive message also. */
    while (pending_send_)
    {
        if (pending_receiving_)
        {
            cond_.wait(lock, [this] { return pending_receiving_; });
            continue;
        }

        if (available_message_ && message_.kind == MessageKind::ACKNOWLEDGE)
        {
            LOG() << "Consuming message: " << message_.desc();
            available_message_ = false;
            pending_send_ = false;
            break;
        }

        pending_receiving_ = true;
        lock.unlock();

        Message msg = {};
        PortError error = impl_->receive(msg);
        if (error != PortError::Ok)
            return error;

        lock.lock();
        pending_receiving_ = false;

        if (msg.kind == MessageKind::ACKNOWLEDGE)
        {
            LOG() << "received <-- ACK: " << msg.desc();
            pending_send_ = false;
            break;
        }

        if (available_message_)
        {
            LOG() << "Dropping message received before acknowledgement: " << msg.desc();
        }
        else
        {
            LOG() << "receiving <-- : " << msg.desc();
            message_ = std::move(msg);
            available_message_ = true;
            cond_.notify_all();
        }
    }
    return PortError::Ok;
}

PortError Port::receive(Message &message)
{
    std::unique_lock<std::mutex> lock{lock_};

    /* If we're waiting for an acknowledgement message, we cannot just
     * wait and we might need to receive message also. */
    for (;;)
    {
        if (available_message_ && message_.kind != MessageKind::ACKNOWLEDGE)
        {
            LOG() << "Consuming message: " << message_.desc();
            message = std::move(message_);
            available_message_ = false;
            return PortError::Ok;
        }

        if (available_message_ && message_.kind == MessageKind::ACKNOWLEDGE)
        {
            LOG() << "Consuming message: " << message_.desc();

            assert(pending_send_); // TODO
            pending_send_ = false;
            available_message_ = false;
            message_ = {};
            continue;
        }

        pending_receiving_ = true;
        lock.unlock();

        /* message_ cannot be modified without locking. */
        Message msg = {};
        PortError error = impl_->receive(msg);

        lock.lock();
        pending_receiving_ = false;

        if (error != PortError::Ok)
            return error;

        LOG() << "Received message: " << msg.desc();
        available_message_ = true;
        message_ = std::move(msg);
        cond_.notify_all();
    }
}

PortError Port::acknowledge(const Message &msg)
{
    Message ack = {};
    ack.kind = MessageKind::ACKNOWLEDGE;
    ack.source = msg.destination;
    ack.destination = msg.source;

    LOG() << "[IPC][ACK] sending acknowledgement message";

    return send(ack);
}

} // namespace ipc
