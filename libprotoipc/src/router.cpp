#include <protoipc/router.hh>

#include <cassert>

#include "logger_utils.h"

namespace ipc
{

RouterBackend::~RouterBackend() = default;
Router::~Router() = default;

Router::Router(std::unique_ptr<RouterBackend> backend) { backend_ = std::move(backend); }

void Router::set_delegate(RouterDelegate *delegate)
{
    std::lock_guard<std::mutex> guard{lock_};
    delegate_ = delegate;
}

std::pair<PortId, std::unique_ptr<Port>> Router::create_port() { return backend_->create_port(); }

bool Router::remove_port(PortId id) { return backend_->remove_port(id); }

ipc::PortError Router::loop()
{

    for (;;)
    {
        LOG() << "Loop iteration: receiving()";
        Message msg;
        PortError err = backend_->receive(msg);
        if (err != PortError::Ok)
            return err;

        std::lock_guard<std::mutex> guard{lock_};
        if (terminating_)
            break;

        if (msg.kind == MessageKind::ACKNOWLEDGE)
        {
            LOG() << "Loop iteration: acknowledgement received";
            auto it = acknowledgements_.find(msg.destination);
            if (it == std::end(acknowledgements_))
            {
                if (delegate_ != nullptr)
                    delegate_->on_message_dropped(std::move(msg));
                continue;
            }

            if (delegate_ != nullptr)
                delegate_->on_message_acked(msg);
            acknowledgements_.erase(it);

            auto destination = msg.destination;
            LOG() << "Loop iteration: retransmitting";
            err = backend_->retransmit(std::move(msg));
            if (err != PortError::Ok)
                return err;

            /* Doesn't work: msg.source will be used as a key instead of destination.
             * We need it that way to continue discarding message by source. */
            auto pending = pendings_.find(destination);
            while (pending != std::end(pendings_))
            {
                LOG() << "Loop iteration: retransmitting pending messsage";
                auto source = pending->second.source;
                acknowledgements_.insert({pending->second.source, pending->second.destination});
                err = backend_->retransmit(std::move(pending->second));
                if (err != PortError::Ok)
                    return err;
                pendings_.erase(pending);
                pendings_ids_.erase(source);
                pending = pendings_.find(destination);
            }
        }
        else if (msg.kind == MessageKind::PAYLOAD)
        {
            /* If the port has already sent an unacknowledged message, this
             * is a protocol error and we can drop the message. */
            auto ack = acknowledgements_.find(msg.source);
            if (ack != std::end(acknowledgements_))
            {
                if (delegate_ != nullptr)
                    delegate_->on_message_dropped(std::move(msg));
                continue;
            }

            /* If the port has already sent an unacknowledged message, this
             * is a protocol error and we can drop the message. */
            auto pending = pendings_ids_.find(msg.source);
            if (pending != std::end(pendings_ids_))
            {
                if (delegate_ != nullptr)
                    delegate_->on_message_dropped(std::move(msg));
                continue;
            }

            /* If the destination port is waiting for an acknowledgement
             * message, we'll not forward the message right away and wait
             * until see the message passing. */
            ack = acknowledgements_.find(msg.destination);
            if (ack != std::end(acknowledgements_))
            {
                assert(pendings_ids_.find(msg.source) == std::end(pendings_ids_));
                pendings_ids_.insert({msg.source, msg.destination});
                pendings_.insert({msg.destination, std::move(msg)});
                LOG() << "Loop iteration: storing pending message";
                continue;
            }

            acknowledgements_.insert({msg.source, msg.destination});
            LOG() << "Loop iteration: retransmitting";
            err = backend_->retransmit(std::move(msg));
            if (err != PortError::Ok)
                return err;
        }
    }

    return ipc::PortError::Ok;
}

void Router::terminate()
{
    backend_->terminate();

    std::lock_guard<std::mutex> guard{lock_};
    terminating_ = true;
}

} // namespace ipc
