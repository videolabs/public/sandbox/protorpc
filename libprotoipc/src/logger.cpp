#include <protoipc/logger.hh>

#include <cstdio>
#include <mutex>

#ifdef _WIN32
#include <windows.h>
#include <synchapi.h>
#else
#include <unistd.h>
#endif

namespace
{

::ipc::Logger *main_logger = nullptr;

class DefaultLogger : public ::ipc::LoggerBackend
{
  public:
    void lock() override
    {
        log_lock.lock();

#ifdef _WIN32
        DWORD pid = GetCurrentProcessId();
        const std::string pid_prefix = "[PID=" + std::to_string(pid) + "]";
#else
        const std::string pid_prefix = "[PID]";
#endif
        std::printf("%s:", pid_prefix.c_str());
    }

    void debug(const std::string &message) override
    {
#ifdef _WIN32
        DWORD pid = GetCurrentProcessId();
        const std::string pid_prefix = "[PID=" + std::to_string(pid) + "]";
#else
        pid_t pid = getpid();
        const std::string pid_prefix = "[PID=" + std::to_string(pid) + "]";
#endif

        if (!m_prefix_sent)
        {
            std::printf("%s: ", pid_prefix.c_str());
            m_prefix_sent = true;
        }

        std::printf("%.*s", (int)message.size(), message.c_str());
    }

    void unlock() override
    {
        if (m_prefix_sent)
            std::printf("\n");
        fflush(stdout);

        m_prefix_sent = false;
        log_lock.unlock();
    }

  private:
    std::mutex log_lock;
    bool m_prefix_sent;
} default_logger_backend;

::ipc::Logger default_logger{default_logger_backend};
} // namespace

namespace ipc
{
Logger &Logger::process_instance()
{
    if (main_logger == nullptr)
        return default_logger;
    return *main_logger;
}

LoggingSession Logger::debug() { return LoggingSession{*m_backend}; }

} // namespace ipc
