#include <protoipc/logger.hh>
#include <protoipc/port.hh>

#include <cassert>
#include <string>

#include "proc_ipc_tests.hh"

int send_receive_message(std::unique_ptr<ipc::Port> &port, int port_id, int other_id)
{
    ipc::Message msg;
    msg.source = port_id;
    msg.destination = other_id;
    msg.payload = {0x41, 0x42, 0x43};

    port->send(msg);
    return 0;
}

int send_payload(std::unique_ptr<ipc::Port> &port, int port_id, int other_id, int payload_size)
{
    ipc::Message msg;
    msg.source = port_id;
    msg.destination = other_id;
    msg.payload = std::vector<std::uint8_t>(payload_size, 0xfe);

    port->send(msg);
    return 0;
}

int send_reply_message_reply(std::unique_ptr<ipc::Port> &port, int port_id)
{
    std::vector<std::uint8_t> reply_payload = {0x43, 0x42, 0x41};

    ipc::Message received;
    ipc::Message reply;

    port->receive(received);

    reply.source = port_id;
    reply.destination = received.source;
    reply.payload = reply_payload;

    port->acknowledge(received);
    port->send(reply);

    return 0;
}

int send_reply_message_send(std::unique_ptr<ipc::Port> &port, int port_id, int other_id)
{
    std::vector<std::uint8_t> payload = {0x41, 0x42, 0x43};

    ipc::Message msg;
    ipc::Message received_reply;

    msg.source = port_id;
    msg.destination = other_id;
    msg.payload = payload;

    port->send(msg);
    port->receive(received_reply);
    port->acknowledge(received_reply);

    return 0;
}

int sending_message_back_and_forth_reply(std::unique_ptr<ipc::Port> &port, int port_id)
{
    std::vector<uint8_t> payload_reply = {0xba, 0xaa, 0xaa, 0xad};

    ipc::Message reply;
    reply.source = port_id;
    reply.payload = payload_reply;

    ipc::Message received;

    for (int it = 0; it < 3; it++)
    {
        port->receive(received);
        reply.destination = received.source;
        port->acknowledge(received);
        port->send(reply);
    }

    return 0;
}

int sending_message_back_and_forth_send(std::unique_ptr<ipc::Port> &port, int port_id, int other_id)
{
    std::vector<uint8_t> payload_msg = {0xde, 0xad, 0xbe, 0xef};

    ipc::Message msg;
    msg.source = port_id;
    msg.destination = other_id;
    msg.payload = payload_msg;

    ipc::Message received;

    for (int it = 0; it < 3; it++)
    {
        port->send(msg);
        port->receive(received);
        port->acknowledge(received);
    }

    return 0;
}

int main(int argc, char **argv)
{
    using ipc::Logger;

    if (argc < 4)
        return -1;
    int ret = 0;
    enum testcase test = static_cast<enum testcase>(std::stoi(argv[1]));
    Handle port_handle = Handle::from_desc(argv[2]);
    std::unique_ptr<ipc::Port> port = ipc::Port::from_handle(port_handle);
    int port_id = std::stoi(argv[3]);

    switch (test)
    {
        case SEND_RECEIVE_MESSAGE:
        {
            int other_id = std::stoi(argv[4]);

            Logger::process_instance().debug() << "SEND_RECEIVE_MESSAGE " << std::to_string(port_id)
                                               << " --> " << std::to_string(other_id);

            ret = send_receive_message(port, port_id, other_id);
            break;
        }
        case SEND_PAYLOAD:
        {
            int other_id = std::stoi(argv[4]);
            int payload_size = std::stoi(argv[5]);

            Logger::process_instance().debug()
                << "SEND_PAYLOAD (sz=" << std::to_string(payload_size) << ") "
                << std::to_string(port_id) << " --> " << std::to_string(other_id);

            ret = send_payload(port, port_id, other_id, payload_size);
            break;
        }
        case SEND_REPLY_MESSAGE_REPLY:
        {
            Logger::process_instance().debug()
                << "SEND_MESSAGE_REPLY from " << std::to_string(port_id);
            ret = send_reply_message_reply(port, port_id);
            break;
        }
        case SEND_REPLY_MESSAGE_SEND:
        {
            int other_id = std::stoi(argv[4]);

            Logger::process_instance().debug()
                << "SEND_REPLY_MESSAGE_SEND " << std::to_string(port_id) << " --> "
                << std::to_string(other_id);

            ret = send_reply_message_send(port, port_id, other_id);
            break;
        }
        case SENDING_MESSAGE_BACK_AND_FORTH_REPLY:
            Logger::process_instance().debug()
                << "SENDING_MESSAGE_BACK_AND_FORTH_REPLY from " << std::to_string(port_id);

            ret = sending_message_back_and_forth_reply(port, port_id);
            break;
        case SENDING_MESSAGE_BACK_AND_FORTH_SEND:
            int other_id = std::stoi(argv[4]);
            Logger::process_instance().debug()
                << "SENDING_MESSAGE_BACK_AND_FORTH_REPLY " << std::to_string(port_id) << " --> "
                << std::to_string(other_id);

            ret = sending_message_back_and_forth_send(port, port_id, other_id);
            break;
    };

    Logger::process_instance().debug() << "[TEST] Waiting for END_CODE";
    ipc::Message received;
    port->receive(received);

    assert(received.payload.size() > 0);
    Logger::process_instance().debug()
        << "[TEST] Receive payload size=" << std::to_string(received.payload.size())
        << ", payload[0]=" << std::to_string(received.payload.front())
        << " (END_CODE=" << std::to_string(END_CODE) << ")";
    port->acknowledge(received);

    assert(received.payload.front() == END_CODE);
    Logger::process_instance().debug() << "[TEST] END_CODE received";

    return ret;
}
