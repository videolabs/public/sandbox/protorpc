#include "proc_ipc_tests.hh"

#include <protoipc/logger.hh>
#include <protoipc/port.hh>
#include <protoipc/router.hh>

#include <utils/process_manager.hh>

#include <gtest/gtest.h>

#include <cstdlib>
#include <cstring>
#include <fcntl.h>
#include <filesystem>
#include <string>
#include <thread>

#if defined(__linux)
const std::filesystem::path client_path =
    std::filesystem::canonical("/proc/self/exe").parent_path() / "proc_ipc_tests_client";
#elif defined(_WIN32)
const std::filesystem::path client_path = "proc_ipc_tests_client.exe";
#endif

struct RouterSetup
{
    RouterSetup()
    {
        router = ipc::Router::make();
        std::tie(client_a_id, client_router_a) = router->create_port();
        std::tie(client_b_id, client_router_b) = router->create_port();
        router_thread = std::thread([&]() {
            router->loop();
            ipc::Logger::process_instance().debug() << "Router loop is finished";
        });
    };

    ~RouterSetup()
    {
        router->terminate();
        router_thread.join();
    };

    std::unique_ptr<ipc::Router> router;
    ipc::PortId client_a_id;
    ipc::PortId client_b_id;
    std::unique_ptr<ipc::Port> client_router_a;
    std::unique_ptr<ipc::Port> client_router_b;
    std::thread router_thread;
};

struct ipc_proc_test : public testing::Test
{
    RouterSetup s;
};

struct SendPayloadFixture : public testing::TestWithParam<std::size_t>
{
    RouterSetup s;
};

TEST_F(ipc_proc_test, send_receive_message)
{
    std::vector<std::uint8_t> payload = {0x41, 0x42, 0x43};

    ipc::Message received;
    ipc::PortError error;

    std::vector<std::string> args = {
        client_path.string(),
        std::to_string(SEND_RECEIVE_MESSAGE),
        s.client_router_a->dup().desc(),
        std::to_string(s.client_a_id),
        std::to_string(s.client_b_id),
    };

    ProcessManager pr(args);

    s.client_router_a->close();
    error = s.client_router_b->receive(received);

    ASSERT_EQ(error, ipc::PortError::Ok);
    ASSERT_EQ(received.source, s.client_a_id);
    ASSERT_EQ(received.destination, s.client_b_id);
    ASSERT_EQ(received.payload, payload);

    ipc::Logger::process_instance().debug() << "[TEST] sending acknowledgement to client b";

    s.client_router_b->acknowledge(received);

    ipc::Logger::process_instance().debug() << "[TEST] sending end message to client b";

    ipc::Message end;
    end.source = s.client_b_id;
    end.destination = s.client_a_id;
    end.payload = {END_CODE};
    error = s.client_router_b->send(end);
    ASSERT_EQ(error, ipc::PortError::Ok);
}

TEST_P(SendPayloadFixture, send_payload)
{
    auto payload = std::vector<std::uint8_t>(GetParam(), 0xfe);

    std::vector<std::string> args = {
        client_path.string(),
        std::to_string(SEND_PAYLOAD),
        s.client_router_a->dup().desc(),
        std::to_string(s.client_a_id),
        std::to_string(s.client_b_id),
        std::to_string(GetParam()),
    };

    ProcessManager pr(args);

    ipc::Message received;
    ipc::PortError error;

    s.client_router_a->close();
    error = s.client_router_b->receive(received);

    ASSERT_EQ(error, ipc::PortError::Ok);
    ASSERT_EQ(received.source, s.client_a_id);
    ASSERT_EQ(received.destination, s.client_b_id);
    ASSERT_EQ(received.payload, payload);
    ASSERT_EQ(received.payload.size(), payload.size());
    s.client_router_b->acknowledge(received);

    ipc::Logger::process_instance().debug() << "[TEST] sending end message to client b";

    ipc::Message end;
    end.source = s.client_b_id;
    end.destination = s.client_a_id;
    end.payload = {END_CODE};
    error = s.client_router_b->send(end);
    ASSERT_EQ(error, ipc::PortError::Ok);
}

INSTANTIATE_TEST_SUITE_P(send_payload_proc,
                         SendPayloadFixture,
                         testing::Values(100, 1000, 50 * 1024 * 1024));

TEST_F(ipc_proc_test, send_reply_message_reply)
{
    // Message from client a to client b
    std::vector<std::uint8_t> payload = {0x41, 0x42, 0x43};
    std::vector<std::uint8_t> reply_payload = {0x43, 0x42, 0x41};

    std::vector<std::string> args = {
        client_path.string(),
        std::to_string(SEND_REPLY_MESSAGE_SEND),
        s.client_router_a->dup().desc(),
        std::to_string(s.client_a_id),
        std::to_string(s.client_b_id),
    };

    ipc::PortError error;
    ipc::Message reply;
    ipc::Message received;
    reply.source = s.client_b_id;
    reply.payload = reply_payload;

    ProcessManager pr(args);

    s.client_router_a->close();

    ipc::Logger::process_instance().debug() << "[TEST] receiving from client b";
    error = s.client_router_b->receive(received);

    ASSERT_EQ(error, ipc::PortError::Ok);
    ASSERT_EQ(received.source, s.client_a_id);
    ASSERT_EQ(received.destination, s.client_b_id);
    ASSERT_EQ(received.payload, payload);
    s.client_router_b->acknowledge(received);

    reply.destination = received.source;

    ipc::Logger::process_instance().debug() << "[TEST] replying to client b";

    error = s.client_router_b->send(reply);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ipc::Logger::process_instance().debug() << "[TEST] sending end message to client b";

    ipc::Message end;
    end.source = s.client_b_id;
    end.destination = s.client_a_id;
    end.payload = {END_CODE};
    error = s.client_router_b->send(end);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ipc::Logger::process_instance().debug() << "[TEST] done";
}

TEST_F(ipc_proc_test, send_reply_message_send)
{
    // Message from client a to client b
    std::vector<std::uint8_t> payload = {0x41, 0x42, 0x43};
    std::vector<std::uint8_t> reply_payload = {0x43, 0x42, 0x41};

    std::vector<std::string> args = {
        client_path.string(),
        std::to_string(SEND_REPLY_MESSAGE_REPLY),
        s.client_router_a->dup().desc(),
        std::to_string(s.client_a_id),
    };

    ipc::Message msg;
    ipc::Message received_reply;
    ipc::PortError error;

    msg.source = s.client_b_id;
    msg.destination = s.client_a_id;
    msg.payload = payload;

    ProcessManager pr(args);

    s.client_router_a->close();
    error = s.client_router_b->send(msg);
    ASSERT_EQ(error, ipc::PortError::Ok);

    error = s.client_router_b->receive(received_reply);
    ASSERT_EQ(error, ipc::PortError::Ok);
    ASSERT_EQ(received_reply.source, s.client_a_id);
    ASSERT_EQ(received_reply.destination, s.client_b_id);
    ASSERT_EQ(received_reply.payload, reply_payload);
    s.client_router_b->acknowledge(received_reply);

    ipc::Message end;
    end.source = s.client_b_id;
    end.destination = s.client_a_id;
    end.payload = {END_CODE};
    error = s.client_router_b->send(end);
    ASSERT_EQ(error, ipc::PortError::Ok);
}

TEST_F(ipc_proc_test, sending_message_and_reply_back_and_forth_send)
{
    std::vector<uint8_t> payload_msg = {0xde, 0xad, 0xbe, 0xef};
    std::vector<uint8_t> payload_reply = {0xba, 0xaa, 0xaa, 0xad};

    std::vector<std::string> args = {
        client_path.string(),
        std::to_string(SENDING_MESSAGE_BACK_AND_FORTH_REPLY),
        s.client_router_a->dup().desc(),
        std::to_string(s.client_a_id),
    };

    ipc::Message msg;
    msg.source = s.client_b_id;
    msg.destination = s.client_a_id;
    msg.payload = payload_msg;

    ipc::Message received;
    ipc::PortError error;

    ProcessManager pr(args);

    s.client_router_a->close();

    for (int it = 0; it < 3; it++)
    {
        s.client_router_b->send(msg);
        error = s.client_router_b->receive(received);

        ASSERT_EQ(error, ipc::PortError::Ok);
        ASSERT_EQ(received.source, s.client_a_id);
        ASSERT_EQ(received.destination, s.client_b_id);
        ASSERT_EQ(received.payload, payload_reply);
        s.client_router_b->acknowledge(received);
    }

    ipc::Message end;
    end.source = s.client_b_id;
    end.destination = s.client_a_id;
    end.payload = {END_CODE};
    error = s.client_router_b->send(end);
    ASSERT_EQ(error, ipc::PortError::Ok);
}

TEST_F(ipc_proc_test, sending_message_and_reply_back_and_forth_reply)
{
    std::vector<uint8_t> payload_msg = {0xde, 0xad, 0xbe, 0xef};
    std::vector<uint8_t> payload_reply = {0xba, 0xaa, 0xaa, 0xad};

    std::vector<std::string> args = {
        client_path.string(),
        std::to_string(SENDING_MESSAGE_BACK_AND_FORTH_SEND),
        s.client_router_a->dup().desc(),
        std::to_string(s.client_a_id),
        std::to_string(s.client_b_id),
    };

    ipc::Message reply;
    reply.source = s.client_b_id;
    reply.payload = payload_msg;

    ipc::Message received;
    ipc::PortError error;

    ProcessManager pr(args);

    s.client_router_a->close();

    for (int it = 0; it < 3; it++)
    {
        error = s.client_router_b->receive(received);

        ASSERT_EQ(error, ipc::PortError::Ok);
        ASSERT_EQ(received.source, s.client_a_id);
        ASSERT_EQ(received.destination, s.client_b_id);
        ASSERT_EQ(received.payload, payload_msg);
        s.client_router_b->acknowledge(received);

        reply.destination = received.source;
        s.client_router_b->send(reply);
    }

    ipc::Message end;
    end.source = s.client_b_id;
    end.destination = s.client_a_id;
    end.payload = {END_CODE};
    error = s.client_router_b->send(end);
    ASSERT_EQ(error, ipc::PortError::Ok);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
