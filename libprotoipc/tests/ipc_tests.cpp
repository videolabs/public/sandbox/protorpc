#include <protoipc/port.hh>
#include <protoipc/router.hh>

#include <gtest/gtest.h>

#include <cstdlib>
#include <cstring>
#include <fcntl.h>
#include <thread>

struct RouterSetup
{
    RouterSetup()
    {
        router = ipc::Router::make();
        router_thread = std::thread([&]() { router->loop(); });
        std::tie(client_a_id, client_router_a) = router->create_port();
        std::tie(client_b_id, client_router_b) = router->create_port();
    };

    ~RouterSetup()
    {
        router->terminate();
        router_thread.join();
    };

    std::unique_ptr<ipc::Router> router;
    ipc::PortId client_a_id;
    ipc::PortId client_b_id;
    std::unique_ptr<ipc::Port> client_router_a;
    std::unique_ptr<ipc::Port> client_router_b;
    std::thread router_thread;
};

struct ipc_test : public testing::Test
{
    RouterSetup s;
};

struct SendPayloadFixture : public testing::TestWithParam<std::size_t>
{
    RouterSetup s;
};

TEST_F(ipc_test, serialize_handles)
{
    Handle orig = s.client_router_a->handle();
    std::string desc = orig.desc();
    Handle handle = Handle::from_desc(desc);
#ifdef _WIN32
    ASSERT_EQ(handle.read, orig.read);
    ASSERT_EQ(handle.write, orig.write);
#else
    ASSERT_EQ(handle.handle, orig.handle);
#endif
    ASSERT_EQ(desc, handle.desc());
}

TEST_F(ipc_test, send_receive_message)
{
    std::vector<std::uint8_t> payload = {0x41, 0x42, 0x43};

    ipc::Message test;
    test.source = s.client_a_id;
    test.destination = s.client_b_id;
    test.payload = payload;

    ipc::PortError error = s.client_router_a->send(test);
    ASSERT_EQ(error, ipc::PortError::Ok);

    // We should receive the message with destination changed to client_a as it
    // passed through the router.
    ipc::Message received;
    error = s.client_router_b->receive(received);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ASSERT_EQ(received.source, s.client_a_id);
    ASSERT_EQ(received.destination, s.client_b_id);
    ASSERT_EQ(received.payload, payload);
    s.client_router_b->acknowledge(received);
}

TEST_P(SendPayloadFixture, send_payload)
{
    ipc::Message test;
    test.source = s.client_a_id;
    test.destination = s.client_b_id;
    test.payload = std::vector<std::uint8_t>(GetParam(), 0xfe);

    ipc::PortError error = s.client_router_a->send(test);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ipc::Message received;
    error = s.client_router_b->receive(received);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ASSERT_EQ(received.source, s.client_a_id);
    ASSERT_EQ(received.destination, s.client_b_id);
    ASSERT_EQ(received.payload, test.payload);
    ASSERT_EQ(received.payload.size(), test.payload.size());
    s.client_router_b->acknowledge(received);
}

INSTANTIATE_TEST_SUITE_P(send_payload,
                         SendPayloadFixture,
                         testing::Values(100, 1000, 50 * 1024 * 1024));

TEST_F(ipc_test, send_reply_message)
{
    // Message from client a to client b
    std::vector<std::uint8_t> payload = {0x41, 0x42, 0x43};

    ipc::Message msg;
    msg.source = s.client_a_id;
    msg.destination = s.client_b_id;
    msg.payload = payload;

    ipc::PortError error = s.client_router_a->send(msg);
    ASSERT_EQ(error, ipc::PortError::Ok);

    // We should receive the message with destination changed to client_a as it
    // passed through the router.
    ipc::Message received;
    error = s.client_router_b->receive(received);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ASSERT_EQ(received.source, s.client_a_id);
    ASSERT_EQ(received.destination, s.client_b_id);
    ASSERT_EQ(received.payload, payload);
    s.client_router_b->acknowledge(received);

    ipc::Message reply;
    reply.source = s.client_b_id;
    reply.destination = received.source;
    reply.payload = {0x43, 0x42, 0x41};

    error = s.client_router_b->send(reply);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ipc::Message received_reply;
    error = s.client_router_a->receive(received_reply);
    ASSERT_EQ(error, ipc::PortError::Ok);

    ASSERT_EQ(received_reply.source, reply.source);
    ASSERT_EQ(received_reply.destination, reply.destination);
    ASSERT_EQ(received_reply.payload, reply.payload);
    s.client_router_a->acknowledge(received_reply);
}

TEST_F(ipc_test, sending_message_and_reply_back_and_forth)
{
    std::vector<uint8_t> payload_msg = {0xde, 0xad, 0xbe, 0xef};
    std::vector<uint8_t> payload_reply = {0xba, 0xaa, 0xaa, 0xad};

    ipc::Message msg;
    msg.source = s.client_a_id;
    msg.destination = s.client_b_id;
    msg.payload = payload_msg;

    ipc::Message reply;
    reply.payload = payload_reply;
    ipc::Message received;

    ipc::PortError error;

    for (int it = 0; it < 3; it++)
    {
        s.client_router_a->send(msg);

        error = s.client_router_b->receive(received);
        ASSERT_EQ(error, ipc::PortError::Ok);

        ASSERT_EQ(received.source, s.client_a_id);
        ASSERT_EQ(received.destination, s.client_b_id);
        ASSERT_EQ(received.payload, payload_msg);
        s.client_router_b->acknowledge(received);

        reply.source = s.client_b_id;
        reply.destination = received.source;
        s.client_router_b->send(reply);

        error = s.client_router_a->receive(received);
        ASSERT_EQ(error, ipc::PortError::Ok);

        ASSERT_EQ(received.source, s.client_b_id);
        ASSERT_EQ(received.destination, s.client_a_id);
        ASSERT_EQ(received.payload, payload_reply);
        s.client_router_a->acknowledge(received);
    }
}

TEST_F(ipc_test, create_after_loop_and_send_request)
{
    std::vector<uint8_t> payload_msg = {0xde, 0xad, 0xbe, 0xef};
    std::vector<uint8_t> payload_reply = {0xba, 0xaa, 0xaa, 0xad};

    /* Exchange a message with the ports added before the router was started to
     * ensure that the router is running and processing everything. */
    {
        ipc::Message msg;
        msg.source = s.client_a_id;
        msg.destination = s.client_b_id;
        msg.payload = payload_msg;

        ipc::Message reply;
        reply.payload = payload_reply;
        ipc::Message received;

        ipc::PortError error;
        s.client_router_a->send(msg);
        error = s.client_router_b->receive(received);
        ASSERT_EQ(error, ipc::PortError::Ok);
        s.client_router_b->acknowledge(received);
    }

    /* Add a new port to check whether the router will read it. */
    auto [new_client_id, new_client] = s.router->create_port();

    /* And finally, exchange a message between new ports to check whether the
     * ports are read by the router. We **DONT** want to use s.client_router_a
     * or s.client_router_b here. */
    ipc::Message msg;
    msg.source = s.client_a_id;
    msg.destination = new_client_id;
    msg.payload = payload_msg;

    s.client_router_a->send(msg);

    ipc::PortError error;
    ipc::Message received;
    error = new_client->receive(received);

    ASSERT_EQ(error, ipc::PortError::Ok);
    ASSERT_EQ(received.source, s.client_a_id);
    ASSERT_EQ(received.destination, new_client_id);
    ASSERT_EQ(received.payload, payload_msg);
    new_client->acknowledge(received);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
