#ifndef linux_proc_ipc_tests_hh_INCLUDED
#define linux_proc_ipc_tests_hh_INCLUDED

#include <cstdint>

static constexpr uint8_t END_CODE = 0x90;

enum testcase
{
    SEND_RECEIVE_MESSAGE,
    SEND_PAYLOAD,
    SEND_REPLY_MESSAGE_REPLY,
    SEND_REPLY_MESSAGE_SEND,
    SENDING_MESSAGE_BACK_AND_FORTH_SEND,
    SENDING_MESSAGE_BACK_AND_FORTH_REPLY
};

#endif // linux_proc_ipc_tests_hh_INCLUDED
