#include <protoipc/logger.hh>
#include <protoipc/message.hh>
#include <protoipc/router.hh>

#include <gtest/gtest.h>

#include <future>
#include <utility>

class TestRouterBackend : public ipc::RouterBackend, public ipc::RouterDelegate
{
  public:
    ipc::PortError retransmit(ipc::Message &&message) override
    {
        ipc::Logger::process_instance().debug()
            << "[TEST] retransmit " << std::to_string(message.source) << " --> "
            << std::to_string(message.destination);

        std::unique_lock guard{lock_};
        cond_retransmit_done_.wait(guard, [this] { return !to_retransmit_.has_value(); });
        to_retransmit_ = message;
        cond_retransmit_.notify_one();
        return ipc::PortError::Ok;
    }

    ipc::PortError receive(ipc::Message &message) override
    {
        std::unique_lock guard{lock_};
        cond_receive_.wait(guard, [this] { return to_receive_.has_value() || terminating_; });

        if (terminating_)
            return ipc::PortError::Unknown;

        message = to_receive_.value();
        to_receive_.reset();
        cond_receive_done_.notify_one();

        ipc::Logger::process_instance().debug()
            << "[TEST] received " << std::to_string(message.source) << " --> "
            << std::to_string(message.destination);

        return ipc::PortError::Ok;
    }

    void on_message_dropped(ipc::Message &&message) override
    {
        ipc::Logger::process_instance().debug()
            << "[ROUTER] message dropped: " << std::to_string(message.source) << " --> "
            << std::to_string(message.destination);

        std::unique_lock guard{lock_};
        assert(!to_receive_.has_value() && !dropped_);
        dropped_ = true;
        cond_retransmit_.notify_one();
    }

    void on_message_acked(const ipc::Message &message) override
    {
        ipc::Logger::process_instance().debug()
            << "[ROUTER] message acked: " << std::to_string(message.source) << " --> "
            << std::to_string(message.destination);
    }

    void test_send(ipc::Message &&message)
    {
        std::unique_lock guard{lock_};
        cond_receive_done_.wait(guard, [this] { return !to_receive_.has_value(); });
        to_receive_ = message;
        cond_receive_.notify_one();
    }

    void assert_dropped()
    {
        std::unique_lock guard{lock_};
        cond_retransmit_.wait(guard, [this] { return to_retransmit_.has_value() || dropped_; });
        assert(!to_retransmit_.has_value() && dropped_);
        dropped_ = false;
    }

    ipc::Message assert_receive()
    {
        std::unique_lock guard{lock_};
        cond_retransmit_.wait(guard, [this] { return to_retransmit_.has_value() || dropped_; });
        assert(to_retransmit_.has_value() && !dropped_);

        ipc::Message msg = to_retransmit_.value();
        to_retransmit_.reset();
        cond_retransmit_done_.notify_one();
        return msg;
    }

    void assert_ack()
    {
        std::unique_lock guard{lock_};
        cond_retransmit_.wait(guard, [this] { return to_retransmit_.has_value() || dropped_; });
        assert(to_retransmit_.has_value() && !dropped_);

        ipc::Message msg = to_retransmit_.value();
        to_retransmit_.reset();
        assert(msg.kind == ipc::MessageKind::ACKNOWLEDGE);
        cond_retransmit_done_.notify_one();
    }

    void wait_receive()
    {
        std::unique_lock guard{lock_};
        cond_receive_done_.wait(guard, [this] { return !to_receive_.has_value(); });
    }

    std::pair<ipc::PortId, std::unique_ptr<ipc::Port>> create_port() override
    {
        return {0, nullptr};
    }

    bool remove_port(ipc::PortId) override { return true; }

    ipc::PortError loop() override { return ipc::PortError::Ok; }

    void terminate() override
    {
        std::unique_lock guard{lock_};
        terminating_ = true;
        cond_receive_.notify_one();
    }

  private:
    bool terminating_ = false;
    bool dropped_ = false;
    std::optional<ipc::Message> to_retransmit_;
    std::optional<ipc::Message> to_receive_;
    std::mutex lock_;
    std::condition_variable cond_receive_;
    std::condition_variable cond_receive_done_;
    std::condition_variable cond_retransmit_;
    std::condition_variable cond_retransmit_done_;
};

static void send_payload(TestRouterBackend *router, std::uint64_t source, std::uint64_t destination)
{
    ipc::Logger::process_instance().debug() << "[TEST] send payload " << std::to_string(source)
                                            << " --> " << std::to_string(destination);

    ipc::Message msg = {};
    msg.kind = ipc::MessageKind::PAYLOAD;
    msg.source = source;
    msg.destination = destination;
    router->test_send(std::move(msg));
}

static void
send_acknowledge(TestRouterBackend *router, std::uint64_t source, std::uint64_t destination)
{
    ipc::Logger::process_instance().debug()
        << "[TEST] send acknowledgement " << std::to_string(source) << " --> "
        << std::to_string(destination);

    ipc::Message msg = {};
    msg.kind = ipc::MessageKind::ACKNOWLEDGE;
    msg.source = source;
    msg.destination = destination;
    router->test_send(std::move(msg));
}

struct RouterSetup
{
    TestRouterBackend *backend;
    ipc::Router router;
    std::future<void> router_future;

    RouterSetup()
        : backend{new TestRouterBackend}
        , router{std::unique_ptr<ipc::RouterBackend>{this->backend}}
    {
        router.set_delegate(backend);
        router_future = std::async(std::launch::async, [this] { router.loop(); });
    }

    ~RouterSetup()
    {
        router.terminate();
        router_future.get();
    }
};

struct router_test : public testing::Test
{
    RouterSetup s;
};

TEST_F(router_test, router_can_receive_message)
{
    {
        send_payload(s.backend, 0, 1);
        ipc::Message msg = s.backend->assert_receive();
        ASSERT_EQ(msg.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg.source, 0);
        ASSERT_EQ(msg.destination, 1);
    }
}

TEST_F(router_test, router_drop_without_acknowledgement)
{
    {
        send_payload(s.backend, 0, 1);
        ipc::Message msg = s.backend->assert_receive();
        ASSERT_EQ(msg.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg.source, 0);
        ASSERT_EQ(msg.destination, 1);
    }

    {
        /* Check that the ACKNOWLEDGE message is expected. */
        send_payload(s.backend, 0, 1);
        s.backend->assert_dropped();

        /* Check that the ACKNOWLEDGE message is still expected after any try. */
        send_payload(s.backend, 0, 1);
        s.backend->assert_dropped();
    }
}

TEST_F(router_test, router_retransmit_after_acknowledgement)
{
    {
        /* Check that we can send a message the other way around. */
        send_payload(s.backend, 1, 0);
        ipc::Message msg = s.backend->assert_receive();
        ASSERT_EQ(msg.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg.source, 1);
        ASSERT_EQ(msg.destination, 0);
    }

    {
        /* Sanity symmetrical check:
         * Check that the ACKNOWLEDGE message is expected. */
        send_payload(s.backend, 1, 0);
        s.backend->assert_dropped();

        /* Check that the ACKNOWLEDGE message is still expected after any try. */
        send_payload(s.backend, 1, 0);
        s.backend->assert_dropped();
    }

    {
        /* Check that we can ACKNOWLEDGE and exchange another message. */
        send_acknowledge(s.backend, 0, 1);
        s.backend->assert_ack();

        send_payload(s.backend, 1, 0);
        ipc::Message msg = s.backend->assert_receive();
        ASSERT_EQ(msg.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg.source, 1);
        ASSERT_EQ(msg.destination, 0);

#if 0
        send_payload(s.backend, 0, 1);
        s.backend->assert_dropped();
        send_payload(s.backend, 1, 0);
        s.backend->assert_dropped();
#endif
    }
}

TEST_F(router_test, router_acknowledgement_multiple_port)
{
    {
        /* Check that a port waiting for acknowledgement will only receive the acknowledgement. */
        send_payload(s.backend, 0, 1);
        ipc::Message msg = s.backend->assert_receive();
        ASSERT_EQ(msg.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg.source, 0);
        ASSERT_EQ(msg.destination, 1);

        /* Check that a port waiting for acknowledgement will only receive the acknowledgement. */
        send_payload(s.backend, 2, 0);
        send_acknowledge(s.backend, 1, 0);
        s.backend->assert_ack();

        msg = s.backend->assert_receive();
        ASSERT_EQ(msg.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg.source, 2);
        ASSERT_EQ(msg.destination, 0);
    }
}

TEST_F(router_test, router_acknowledgement_multiple_port_simultaneous)
{
    {
        /* Check that a port waiting for acknowledgement will only receive the acknowledgement. */
        send_payload(s.backend, 0, 1);
        ipc::Message msg = s.backend->assert_receive();
        ASSERT_EQ(msg.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg.source, 0);
        ASSERT_EQ(msg.destination, 1);

        /* Check that a port waiting for acknowledgement will only receive the acknowledgement. */
        send_payload(s.backend, 2, 0);
        send_payload(s.backend, 3, 0);
        send_acknowledge(s.backend, 1, 0);
        s.backend->assert_ack();

        auto msg1 = s.backend->assert_receive();
        auto msg2 = s.backend->assert_receive();

        if (msg1.source == 3)
            std::swap(msg1, msg2);

        ASSERT_EQ(msg1.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg1.source, 2);
        ASSERT_EQ(msg1.destination, 0);

        ASSERT_EQ(msg2.kind, ipc::MessageKind::PAYLOAD);
        ASSERT_EQ(msg2.source, 3);
        ASSERT_EQ(msg2.destination, 0);
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
