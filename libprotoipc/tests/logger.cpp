#include <protoipc/logger.hh>

#include "gtest/gtest.h"

#include <cassert>

class TestLogger : public ipc::LoggerBackend
{
  public:
    bool locked;
    std::string locked_message;
    std::string previous_message;

    void lock() override
    {
        assert(!locked);
        locked = true;
        locked_message = std::string();
    }

    void unlock() override
    {
        assert(locked);
        locked = false;
        previous_message = locked_message;
    }

    void debug(const std::string &msg) override
    {
        assert(locked);
        locked_message += msg;
    }
} test_logger;

/**
 * Check that the ipc::LoggerBackend is correctly locked by the
 * ipc::LoggingSession and correctly unlocked when the session is
 * destroyed. Then check that a message can still be sent when a new
 * session is initiated by `logger.debug()`.
 */
TEST(ipc_test, send_message_logger)
{
    ipc::Logger logger(test_logger);

    ASSERT_EQ(test_logger.locked, false);
    {
        auto &&session = logger.debug();
        ASSERT_EQ(test_logger.locked, true);
    }
    ASSERT_EQ(test_logger.locked, false);

    const std::string prefix = std::string(__FILE__) + ":" + std::to_string(__LINE__);
    logger.debug() << prefix << ": test logger"
                   << " is working";

    ASSERT_EQ(test_logger.previous_message, prefix + ": test logger is working");
}

/**
 * Check that we can get the ipc::LoggingSession from a function, and
 * then move it to another session variable without crashing.
 */
TEST(ipc_test, move_session)
{
    ipc::Logger logger(test_logger);

    const std::string prefix = std::string(__FILE__) + ":" + std::to_string(__LINE__);
    {
        auto session = [&logger] { return logger.debug(); }();
        auto session2 = std::move(session);

        session2 << prefix << ": test logger"
                 << " is working";
    }

    ASSERT_EQ(test_logger.previous_message, prefix + ": test logger is working");
}

/**
 * We check that we can send multiple logs to the default logger.
 **/
TEST(ipc_test, use_default_logger)
{
    using ipc::Logger;

    Logger::process_instance().debug() << "test logger";

    Logger::process_instance().debug() << "test logger";
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
