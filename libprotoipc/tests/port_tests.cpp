#include <protoipc/logger.hh>
#include <protoipc/message.hh>
#include <protoipc/port.hh>

#include <gtest/gtest.h>

#include <future>

class TestPortBackend : public ipc::PortBackend
{
  public:
    ipc::PortError send(const ipc::Message &message) override
    {
        std::unique_lock<std::mutex> guard{lock_};
        assert(!send_request.request);
        send_request.request = true;
        request_cond_.notify_one();

        ipc::Logger::process_instance().debug() << "   TestBackend: waiting send()";

        reply_cond_.wait(guard, [this] { return send_request.ready; });

        ipc::Logger::process_instance().debug() << "   TestBackend:         send() -> done";

        send_request.ready = false;
        send_request.request = false;
        return ipc::PortError::Ok;
    }

    ipc::PortError receive(ipc::Message &message) override
    {
        std::unique_lock<std::mutex> guard{lock_};
        assert(!receive_request.request);
        receive_request.request = true;
        receive_request.msg = message;
        request_cond_.notify_one();

        ipc::Logger::process_instance().debug() << "   TestBackend: waiting receive()";

        reply_cond_.wait(guard, [this] { return receive_request.ready; });

        ipc::Logger::process_instance().debug() << "   TestBackend:         receive() -> done";

        receive_request.ready = false;
        receive_request.request = false;
        message = std::move(receive_request.msg);
        return ipc::PortError::Ok;
    }

    ipc::Handle handle() override { return {}; }

    ipc::Handle dup() override { return {}; }

    void close() override {}

    int notify() override { return 0; }

    bool requesting()
    {
        return (receive_request.request && !receive_request.ready) ||
               (send_request.request && !send_request.ready);
    }

    std::mutex lock_;
    std::condition_variable request_cond_;
    std::condition_variable reply_cond_;

    struct
    {
        bool ready = false;
        bool request = false;
        ipc::Message msg;
    } send_request;

    struct
    {
        bool ready = false;
        bool request = false;
        ipc::Message msg;
    } receive_request;
};

TEST(port_test, port_acknowledgement_logic)
{
    TestPortBackend *impl = new TestPortBackend;
    ipc::Port port1{std::unique_ptr<ipc::PortBackend>(impl)};

    ipc::Logger::process_instance().debug() << " - Running first send request synchronously";

    auto send_future = std::async(std::launch::async, [&port1] {
        ipc::Logger::process_instance().debug() << " - Running the send request";
        ipc::Message msg = {};
        msg.kind = ipc::MessageKind::PAYLOAD;
        return port1.send(msg);
    });

    {
        ipc::Logger::process_instance().debug() << " - Execute first send request";
        std::unique_lock<std::mutex> guard{impl->lock_};
        impl->request_cond_.wait(guard, [impl] { return impl->requesting(); });
        impl->send_request.ready = true;
        impl->reply_cond_.notify_one();
    }
    ASSERT_EQ(send_future.get(), ipc::PortError::Ok);

    ipc::Logger::process_instance().debug() << " - Running asynchronous send request";
    send_future = std::async(std::launch::async, [&port1] {
        ipc::Message msg = {};
        msg.kind = ipc::MessageKind::PAYLOAD;
        return port1.send(msg);
    });

    {
        std::unique_lock<std::mutex> guard{impl->lock_};

        ipc::Logger::process_instance().debug() << " - Waiting for receive request";

        impl->request_cond_.wait(guard, [impl] { return impl->requesting(); });
        ASSERT_FALSE(impl->send_request.request);
        ASSERT_TRUE(impl->receive_request.request);

        ipc::Logger::process_instance().debug() << " - Sending ack message";

        ipc::Message ack = {};
        ack.kind = ipc::MessageKind::ACKNOWLEDGE;
        impl->receive_request.msg = std::move(ack);
        impl->receive_request.ready = true;
        impl->reply_cond_.notify_one();

        ipc::Logger::process_instance().debug() << " - Waiting for send request";

        impl->request_cond_.wait(guard, [impl] { return impl->requesting(); });
        ASSERT_TRUE(impl->send_request.request);
        ASSERT_FALSE(impl->receive_request.request);

        ipc::Logger::process_instance().debug() << " - Running last send request";
        impl->send_request.ready = true;
        impl->reply_cond_.notify_one();
    }

    ipc::Logger::process_instance().debug()
        << " - Waiting for the second send request to terminate";
    ASSERT_EQ(send_future.get(), ipc::PortError::Ok);
}

TEST(port_test, port_acknowledgement_while_receiving)
{
    TestPortBackend *impl = new TestPortBackend;
    ipc::Port port1{std::unique_ptr<ipc::PortBackend>(impl)};

    ipc::Logger::process_instance().debug() << " - Running first send request synchronously";

    auto send_future = std::async(std::launch::async, [&port1] {
        ipc::Logger::process_instance().debug() << " - Running the send request";
        ipc::Message msg = {};
        msg.kind = ipc::MessageKind::PAYLOAD;
        return port1.send(msg);
    });

    {
        ipc::Logger::process_instance().debug() << " - Execute first send request";
        std::unique_lock<std::mutex> guard{impl->lock_};
        impl->request_cond_.wait(guard, [impl] { return impl->requesting(); });
        impl->send_request.ready = true;
        impl->reply_cond_.notify_one();
    }
    ASSERT_EQ(send_future.get(), ipc::PortError::Ok);

    auto receive_future = std::async(std::launch::async, [&port1] {
        ipc::Logger::process_instance().debug() << " - Running asynchronous receive request";
        ipc::Message msg;
        return port1.receive(msg);
    });

    {
        std::unique_lock<std::mutex> guard{impl->lock_};
        impl->request_cond_.wait(guard, [impl] {
            ipc::Logger::process_instance().debug() << " - Wait for the receive request";
            return impl->receive_request.request && !impl->receive_request.ready;
        });
        ipc::Logger::process_instance().debug() << " - Wait for the receive request -> done";
    }

    /* The send request will get stuck waiting for a message **without**
     * calling TestPortBackend::send() or TestPortBackend::receive() because
     * Port::receive() is still pending, and Port::send() will be waiting
     * for an ACKNOWLEDGE message. */
    send_future = std::async(std::launch::async, [&port1] {
        ipc::Logger::process_instance().debug() << " - Running asynchronous send request";
        ipc::Message msg = {};
        msg.kind = ipc::MessageKind::PAYLOAD;
        return port1.send(msg);
    });

    /* TODO: we cannot be sure the send() has been done, but that's ok for
     *       the test to pass since we want to check ACKNOWLEDGE on receive(). */
    {
        std::unique_lock<std::mutex> guard{impl->lock_};
        ASSERT_FALSE(impl->send_request.request);
        ASSERT_TRUE(impl->receive_request.request);

        ipc::Logger::process_instance().debug() << " - Sending ack message";

        impl->request_cond_.wait(guard, [impl] { return impl->receive_request.request; });

        ipc::Message ack = {};
        ack.kind = ipc::MessageKind::ACKNOWLEDGE;
        impl->receive_request.msg = std::move(ack);
        impl->receive_request.ready = true;
        impl->reply_cond_.notify_all();

        ipc::Logger::process_instance().debug() << " - Waiting for requests to complete";

        impl->request_cond_.wait(guard, [impl] { return !impl->receive_request.ready; });

        ipc::Logger::process_instance().debug() << " - Completing send request";

        impl->send_request.ready = true;
        impl->reply_cond_.notify_all();
    }
    ASSERT_EQ(send_future.get(), ipc::PortError::Ok);

    {
        std::unique_lock<std::mutex> guard{impl->lock_};
        ipc::Logger::process_instance().debug()
            << " - Waiting : receive request should be re-iterated";
        impl->request_cond_.wait(guard, [impl] { return impl->requesting(); });
        ASSERT_FALSE(impl->send_request.request);
        ASSERT_TRUE(impl->receive_request.request);

        ipc::Message reply = {};
        reply.kind = ipc::MessageKind::PAYLOAD;
        ipc::Logger::process_instance().debug() << " - Running last receive request";
        impl->receive_request.msg = std::move(reply);
        impl->receive_request.ready = true;
        impl->reply_cond_.notify_all();
    }

    ipc::Logger::process_instance().debug() << " - Waiting for the receive request to terminate";
    ASSERT_EQ(receive_future.get(), ipc::PortError::Ok);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
