#ifndef IPC_MESSAGE_HH
#define IPC_MESSAGE_HH

#include <protoipc/handles.hh>

#include <cstdint>
#include <string>
#include <vector>

namespace ipc
{

static constexpr auto IPC_MESSAGE_HEADER_LEN = 5u;

enum class MessageKind : uint64_t
{
    PAYLOAD,
    ACKNOWLEDGE,
};

/**
 * Abstraction over data sent over ports. Contains enough information
 * to be routed without having to parse the payload.
 */
struct Message
{
    MessageKind kind = MessageKind::PAYLOAD;
    std::uint64_t source = 0;
    std::uint64_t destination = 0;
    std::vector<std::uint8_t> payload;
    std::vector<Handle> handles;

    std::string desc() const
    {
        return std::to_string((int)kind) + " " + std::to_string(payload.size()) + " " +
               std::to_string(handles.size()) + " " + std::to_string(source) + " " +
               std::to_string(destination);
    }
};

} // namespace ipc

#endif
