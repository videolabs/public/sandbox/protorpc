#ifndef IPC_LOGGER_HH
#define IPC_LOGGER_HH

#include <protoipc/common.h>

#include <string>

namespace ipc
{

/**
 * Backend implementation for where the logger will log to.
 */
class PROTOIPC_EXPORT LoggerBackend
{
  public:
    virtual ~LoggerBackend() = default;

    /**
     * Lock the logging system to push multiple message.
     *
     * It's the user responsibility to unlock the logger using
     * Logger::unlock() afterwards.
     */
    virtual void lock() = 0;

    /**
     * Unlock the logging system.
     *
     * The logger must have been locked with Logger::lock() before.
     */
    virtual void unlock() = 0;

    virtual void debug(const std::string &message) = 0;
};

/**
 * Allow to supply a log to the Logger in multiple call.
 */
class PROTOIPC_EXPORT LoggingSession
{
  public:
    LoggingSession(LoggerBackend &backend)
    {
        m_backend = &backend;
        m_backend->lock();
    }

    LoggingSession(LoggingSession &&session)
    {
        m_backend = session.m_backend;
        session.m_backend = nullptr;
    }

    ~LoggingSession()
    {
        if (m_backend == nullptr)
            return;

        m_backend->unlock();
    }

    void append(const std::string &message) { m_backend->debug(message); }

  private:
    LoggerBackend *m_backend;
};

template <typename T> LoggingSession &operator<<(LoggingSession &session, T &&msg)
{
    session.append(msg);
    return session;
}

template <typename T> LoggingSession &operator<<(LoggingSession &&session, T &&msg)
{
    session.append(msg);
    return session;
}

/**
 * Client for the logger usage.
 *
 * The clients using logging can get the process logger using the static
 * method Logger::process_instance() and then use this class to get a
 * LoggingSession through Logger::debug().
 **/
class PROTOIPC_EXPORT Logger
{
  public:
    /**
     * Construct a logger on an existing LogggerBackend.
     *
     * The backend must stay alive during the whole lifetime of the Logger
     * object.
     **/
    Logger(LoggerBackend &backend) : m_backend(&backend) {}

    /**
     * Get the current logger configured for the process.
     **/
    static Logger &process_instance();

    /**
     * Start a logging debug session.
     *
     * Only a single session can be active per Logger and LoggerBackend.
     **/
    LoggingSession debug();

  private:
    LoggerBackend *m_backend;
};

} // namespace ipc

#endif
