#ifndef IPC_LINUX_PORT_HH
#define IPC_LINUX_PORT_HH

#include <protoipc/port.hh>

namespace ipc
{
class LinuxPort : public PortBackend
{
  public:
    LinuxPort();
    LinuxPort(Handle handle);
    ~LinuxPort();

    PortError send(const Message &message) override;
    PortError receive(Message &message) override;

    Handle handle() override { return Handle{pipe_fd_}; }

    Handle dup() override;

    void close() override;
    int notify() override;

  private:
    int pipe_fd_;
    int event_fd_;
};

} // namespace ipc
#endif // IPC_LINUX_PORT_HH
