#ifndef IPC_LINUX_ROUTER_HH
#define IPC_LINUX_ROUTER_HH

#include <protoipc/linux/port.hh>
#include <protoipc/router.hh>

#include <mutex>
#include <unordered_map>

namespace ipc
{

class LinuxRouter : public RouterBackend
{
  public:
    LinuxRouter();
    ~LinuxRouter() override;

    std::pair<PortId, std::unique_ptr<Port>> create_port() override;

    bool remove_port(PortId id) override;

    void terminate() override;

    PortError receive(Message &message) override;
    PortError retransmit(Message &&message) override;

    ipc::PortError loop() override;

  private:
    PortId add_port(int fd);
    PortId current_id_ = 0;
    std::atomic_bool end_ = false;
    std::unordered_map<PortId, std::unique_ptr<LinuxPort>> ports_;
    int epoll_fd_ = -1;
    int event_fd_ = -1;
    std::mutex lock_;
};
} // namespace ipc
#endif // IPC_LINUX_ROUTER_HH
