#ifndef IPC_PORT_HH
#define IPC_PORT_HH

#include <protoipc/common.h>
#include <protoipc/handles.hh>
#include <protoipc/logger.hh>
#include <protoipc/message.hh>

#include <condition_variable>
#include <memory>
#include <mutex>

namespace ipc
{

enum class PortError
{
    Ok = 0,
    IncompleteMessage,
    TooManyHandles,
    ReadFailed,
    WriteFailed,
    BadFileDescriptor,
    PollError,
    ConnectionClosed,
    Unknown
};

class PROTOIPC_EXPORT PortBackend
{
  public:
    virtual ~PortBackend() = default;

    virtual PortError send(const Message &message) = 0;
    virtual PortError receive(Message &message) = 0;

    virtual Handle handle() = 0;
    virtual Handle dup() = 0;

    virtual void close() = 0;
    virtual int notify() = 0;
};

/**
 * Abstraction over platform specific ipc mechanism.
 */
class PROTOIPC_EXPORT Port
{
  public:
    Port(std::unique_ptr<PortBackend> impl);

    PortError send(const Message &message);
    PortError receive(Message &message);

    PortError acknowledge(const Message &msg);
    PortError receive_ack();

    void close() { impl_->close(); }

    int notify() { return impl_->notify(); }

    Handle dup() { return impl_->dup(); }

    Handle handle() { return impl_->handle(); }

    static std::unique_ptr<Port> from_handle(Handle handle);

  private:
    std::unique_ptr<PortBackend> impl_;
    std::mutex lock_;
    std::condition_variable cond_;
    bool pending_send_ = false;
    bool pending_receiving_ = false;
    bool available_message_ = false;
    Message message_;
};
} // namespace ipc

#endif
