#ifndef IPC_COMMON_H
#define IPC_COMMON_H

#ifdef _WIN32
#define PROTOIPC_EXPORT __declspec(dllexport)
#elif defined(__GNUC__)
#define PROTOIPC_EXPORT __attribute__((visibility("default")))
#else
#define PROTOIPC_EXPORT
#endif

#endif
