#ifndef IPC_HANDLE_HH
#define IPC_HANDLE_HH

#include <string>

#if defined(_WIN32)
#include <windows.h>

#include <cinttypes>
#include <iomanip>
#endif

namespace ipc
{

struct Handle
{
#if defined(_WIN32)
    HANDLE read;
    HANDLE write;

    std::string desc()
    {
        uint64_t r_handle = reinterpret_cast<uint64_t>(read);
        uint64_t w_handle = reinterpret_cast<uint64_t>(write);
        return std::to_string(r_handle) + ":" + std::to_string(w_handle);
    }

    static Handle from_desc(std::string desc)
    {
        auto delimiter = desc.find(":");
        if (delimiter == desc.length())
            return Handle{INVALID_HANDLE_VALUE, INVALID_HANDLE_VALUE};
        // TODO CHECK delimiter

        std::string r_handle = desc.substr(0, delimiter);
        std::string w_handle = desc.substr(delimiter + 1);

        return Handle{
            reinterpret_cast<HANDLE>(std::stoi(r_handle)),
            reinterpret_cast<HANDLE>(std::stoi(w_handle)),
        };
    }

#else
    int handle;

    std::string desc() { return std::to_string(handle); }

    static Handle from_desc(std::string desc) { return Handle{std::stoi(desc)}; }
#endif
};

} /* namespace ipc */

using ipc::Handle;

#endif
