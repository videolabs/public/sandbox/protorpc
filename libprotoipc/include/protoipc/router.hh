#ifndef IPC_ROUTER_HH
#define IPC_ROUTER_HH

#include <protoipc/common.h>
#include <protoipc/port.hh>

#include <atomic>
#include <cstdint>
#include <memory>
#include <unordered_map>

namespace ipc
{
using PortId = std::uint64_t;
class Port;
class Router;

class PROTOIPC_EXPORT RouterDelegate
{
  public:
    virtual ~RouterDelegate() = default;

    virtual void on_message_dropped(ipc::Message &&message) {}

    virtual void on_message_acked(const ipc::Message &ack_message) {}
};

class PROTOIPC_EXPORT RouterBackend
{
  public:
    virtual ~RouterBackend();

    /**
     * Create a new pair of port. Returns the PortId associated with the watched
     * port and return the other end of the communication
     */
    virtual std::pair<PortId, std::unique_ptr<Port>> create_port() = 0;

    /**
     * Removes a destination port from the router. Returns true if port could
     * be removed.
     */
    virtual bool remove_port(PortId id) = 0;

    /**
     * Handles requests and routes messages. Messages with unknown desintation
     * are dropped.
     */
    virtual ipc::PortError loop() = 0;

    virtual void terminate() = 0;

    virtual PortError retransmit(Message &&message) = 0;
    virtual PortError receive(Message &message) = 0;
};

/**
 * Central node of the ipc layer. It routes ipc::Messages between multiple
 * ipc::Ports.
 */
class PROTOIPC_EXPORT Router
{
  public:
    Router() = default;
    Router(std::unique_ptr<RouterBackend> backend);

    ~Router();

    void set_delegate(RouterDelegate *delegate);

    /**
     * Create a new pair of port. Returns the PortId associated with the watched
     * port and return the other end of the communication
     */
    std::pair<PortId, std::unique_ptr<Port>> create_port();

    /**
     * Removes a destination port from the router. Returns true if port could
     * be removed.
     */
    bool remove_port(PortId id);

    /**
     * Handles requests and routes messages. Messages with unknown desintation
     * are dropped.
     */
    ipc::PortError loop();
    void terminate();

    static std::unique_ptr<Router> make();

  private:
    std::mutex lock_;
    RouterDelegate *delegate_ = nullptr;
    std::unique_ptr<RouterBackend> backend_;
    std::unordered_map<PortId, PortId> acknowledgements_;
    std::unordered_map<PortId, PortId> pendings_ids_;
    std::unordered_multimap<PortId, Message> pendings_;

    bool terminating_ = false;
};
} // namespace ipc

#endif
