#ifndef RPC_CHANNEL_HH
#define RPC_CHANNEL_HH

#include <protoipc/port.hh>
#include <protorpc/common.h>
#include <protorpc/message.hh>
#include <protorpc/rpcobject.hh>

#include <atomic>
#include <condition_variable>
#include <deque>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <unordered_set>

namespace rpc
{
class PROTORPC_EXPORT Channel
{
  public:
    struct PendingRpcMessage
    {
        PortId source_port;
        ObjectId destination_object;
        rpc::Message message;
    };

    Channel(PortId port_id, std::unique_ptr<ipc::Port> port);

    ~Channel() { request_handling_thread_.join(); }

    /**
     * Binds a receiver to the current channel and return its id.
     */
    template <typename T, typename... Ts> ObjectId bind(Ts &&...args)
    {
        next_id_();
        Receiver<T> object = std::make_shared<T>(std::forward<Ts>(args)...);

        {
            std::lock_guard<std::mutex> lk(lock_);
            receivers_[current_id_] = object;
        }
        allocated_objects_.emplace(current_id_);

        return current_id_;
    }

    /**
     * Binds a receiver to the current channel with a specific id, overwriting the previous
     * object if it exists.
     */
    template <typename T, typename... Ts> void bind_static(ObjectId id, Ts &&...args)
    {
        Receiver<T> object = std::make_shared<T>(std::forward<Ts>(args)...);

        {
            std::lock_guard<std::mutex> lk(lock_);
            receivers_[id] = object;
        }
        allocated_objects_.emplace(id);
    }

    /**
     * Binds a proxy to the current channel.
     */
    template <typename T> Proxy<T> connect(PortId remote_port, ObjectId remote_id)
    {
        next_id_();
        Proxy<T> object = std::make_shared<T>(this, current_id_, remote_port, remote_id);
        allocated_objects_.emplace(current_id_);
        return object;
    }

    /**
     * Send an unidirectional message to a remote object.
     */
    bool send_message(PortId remote_port, rpc::Message &msg);

    /**
     * Sends a bidirectional message to a remote object. Blocks the event loop while
     * waiting for an answer.
     */
    bool send_request(PortId remote_port, rpc::Message &msg, rpc::Message &result);

    /**
     * Handles the event loop.
     */
    ipc::PortError loop();

    void stop()
    {
        port_->notify();
        finished_.store(true);

        /* We need to lock to signal the condition variable here,
         * otherwise they might miss the finished_ value and at
         * the same time miss the signal. */
        std::unique_lock<std::mutex> lk(lock_);
        result_cv_.notify_all();
        request_cv_.notify_all();
    }

  private:
    /**
     * Advance to the next available id.
     */
    void next_id_();

    /**
     * Handles a request
     */
    void request_handling_loop_();
    std::thread request_handling_thread_;
    std::deque<PendingRpcMessage> request_queue_;
    std::condition_variable request_cv_;

    PortId port_id_;
    std::unique_ptr<ipc::Port> port_;
    ObjectId current_id_ = 0;

    std::unordered_map<ObjectId, std::shared_ptr<RpcReceiver>> receivers_;

    /**
     * Keeps the id of all allocated objects. Needed for computing the next available
     * id.
     */
    std::unordered_set<ObjectId> allocated_objects_;

    /**
     * Extracts the payload from the ipc message and does preprocessing on the
     * rpc message (swapping source and destination object ids).
     */
    ipc::PortError next_message_(PendingRpcMessage &pending);

    std::mutex lock_;
    std::condition_variable result_cv_;
    std::condition_variable message_cv;
    rpc::Message current_pending_result_;
    std::atomic_bool finished_;

    bool has_message = false;
};
} // namespace rpc

#endif
