#ifndef RPC_COMMON_H
#define RPC_COMMON_H

#ifdef _WIN32
#define PROTORPC_EXPORT __declspec(dllexport)
#elif defined(__GNUC__)
#define PROTORPC_EXPORT __attribute__((visibility("default")))
#else
#define PROTORPC_EXPORT
#endif

#endif
