#ifndef RPC_MESSAGE_HH
#define RPC_MESSAGE_HH

#include <protoipc/handles.hh>

#include <atomic>
#include <cstdint>
#include <vector>

namespace rpc
{

enum class MessageType : std::uint8_t
{
    Request = 0,
    Result
};

struct Message
{
    // Source object
    std::uint64_t source;

    // Destination object
    std::uint64_t destination;

    // Operation on the object
    std::uint64_t opcode;

    // rpc message content
    std::vector<std::uint8_t> payload;

    // File descriptors
    std::vector<Handle> handles;

    // session_id
    std::uint32_t session_id;

    // message type
    MessageType type;

    void nextid()
    {
        static std::atomic<std::uint32_t> id = 1;
        session_id = id++;
    }
};

} // namespace rpc

#endif
