#ifndef linux_proc_rpc_simple_tests_hh_INCLUDED
#define linux_proc_rpc_simple_tests_hh_INCLUDED

#include <cstdint>

constexpr std::uint64_t PING_COMMAND = 42;
constexpr std::uint64_t SHUTDOWN_COMMAND = 0xdead;

#endif // linux_proc_rpc_simple_tests_hh_INCLUDED
