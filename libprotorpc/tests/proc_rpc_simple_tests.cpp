#include "proc_rpc_simple_tests.hh"

#include <protoipc/port.hh>
#include <protoipc/router.hh>
#include <protorpc/channel.hh>
#include <protorpc/message.hh>
#include <protorpc/rpcobject.hh>
#include <protorpc/serializer.hh>
#include <protorpc/unserializer.hh>

#include <utils/process_manager.hh>

#include <fmt/core.h>
#include <gtest/gtest.h>

#include <filesystem>
#include <thread>

#if defined(__linux)
std::filesystem::path server_path =
    std::filesystem::canonical("/proc/self/exe").parent_path() / "proc_rpc_simple_tests_server";
#elif defined(_WIN32)
const std::filesystem::path server_path = "proc_rpc_simple_tests_server.exe";
#endif

class SimpleSendProxy : public rpc::RpcProxy
{
  public:
    SimpleSendProxy(rpc::Channel *chan,
                    rpc::ObjectId object_id,
                    rpc::PortId remote_port,
                    rpc::ObjectId remote_id)
        : rpc::RpcProxy(chan, object_id, remote_port, remote_id)
    {}

    std::string ping(std::string ping_str)
    {
        std::string output;
        rpc::Message message;
        message.source = id();
        message.destination = remote_id();
        message.opcode = PING_COMMAND;
        message.type = rpc::MessageType::Request;
        message.nextid();

        rpc::Serializer s;
        s.serialize(ping_str);

        message.payload = s.get_payload();

        rpc::Message result;

        if (!channel_->send_request(remote_port(), message, result))
            throw std::runtime_error("[PROXY] There was an error when sending the ping request\n");

        rpc::Unserializer u(std::move(result.payload));

        if (!u.unserialize(&output))
            throw std::runtime_error("There was an error parsing the ping reply");

        return output;
    }

    bool shutdown()
    {
        rpc::Message message;
        message.source = id();
        message.destination = remote_id();
        message.opcode = SHUTDOWN_COMMAND;
        message.type = rpc::MessageType::Request;
        message.nextid();

        if (!channel_->send_message(remote_port(), message))
        {
            fmt::print("[PROXY] There was an error when sending the shutdown request\n");
            return false;
        }
        return true;
    }
};

struct RPCSetup
{
    RPCSetup()
    {
        router = ipc::Router::make();
        std::tie(client_a_id, client_router_a) = router->create_port();
        std::tie(client_b_id, client_router_b) = router->create_port();
        router_thread = std::thread([&]() { router->loop(); });

        emitter = std::make_unique<rpc::Channel>(client_a_id, std::move(client_router_a));
        emitter_thread = std::thread([&]() { emitter->loop(); });
    };

    ~RPCSetup()
    {
        emitter->stop();
        emitter_thread.join();
        router->terminate();
        router_thread.join();
    };

    std::unique_ptr<ipc::Router> router;
    ipc::PortId client_a_id;
    ipc::PortId client_b_id;
    std::unique_ptr<ipc::Port> client_router_a;
    std::unique_ptr<ipc::Port> client_router_b;
    std::thread router_thread;

    std::unique_ptr<rpc::Channel> emitter;
    std::thread emitter_thread;
};

struct rpc_proc_test : public testing::Test
{
    RPCSetup s;
};

TEST_F(rpc_proc_test, simple_send)
{
    bool result;

    auto proxy = s.emitter->connect<SimpleSendProxy>(s.client_b_id, 0);

    std::vector<std::string> args = {
        server_path.string(),
        s.client_router_b->dup().desc(),
        std::to_string(s.client_b_id),
    };

    ProcessManager pj(args);

    std::string ping_string = "7253c09bd391db2cd370455fc64e520ac79fca31";
    std::string pong_string;

    pong_string = proxy->ping(ping_string);
    ASSERT_EQ(pong_string, ping_string);

    result = proxy->shutdown();
    ASSERT_EQ(result, true);
}

TEST_F(rpc_proc_test, simple_multi_proxy)
{
    bool result;

    auto first_proxy = s.emitter->connect<SimpleSendProxy>(s.client_b_id, 0);
    auto second_proxy = s.emitter->connect<SimpleSendProxy>(s.client_b_id, 0);

    ASSERT_NE(first_proxy->id(), second_proxy->id());

    std::vector<std::string> args = {
        server_path.string(),
        s.client_router_b->dup().desc(),
        std::to_string(s.client_b_id),
    };

    ProcessManager pj(args);

    // First proxy
    std::string ping_string = "7253c09bd391db2cd370455fc64e520ac79fca31";
    std::string pong_string;

    pong_string = first_proxy->ping(ping_string);
    ASSERT_EQ(pong_string, ping_string);

    // Second proxy
    ping_string = "8642f7ca4cbc8318b82d44b07e593a49fd35c4bc";
    pong_string = "";

    pong_string = second_proxy->ping(ping_string);
    ASSERT_EQ(pong_string, ping_string);

    result = first_proxy->shutdown();
    ASSERT_EQ(result, true);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
