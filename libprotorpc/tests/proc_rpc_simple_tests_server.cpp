#include <protoipc/port.hh>
#include <protorpc/channel.hh>
#include <protorpc/serializer.hh>
#include <protorpc/unserializer.hh>

#include <fmt/core.h>

#include <string.h>

#include "proc_rpc_simple_tests.hh"

class SimpleSendReceiver : public rpc::RpcReceiver
{
  public:
    void on_message(rpc::Channel &chan,
                    rpc::ObjectId object,
                    rpc::PortId source_port,
                    rpc::Message &message) override
    {
        if (message.opcode == PING_COMMAND)
        {
            fmt::print("Received PING from {},{}\n", source_port, message.destination);

            rpc::Unserializer u(std::move(message.payload));
            std::string ping_str;

            if (!u.unserialize(&ping_str))
                throw std::runtime_error("Could not unserialize ping string");

            rpc::Serializer s;
            s.serialize(ping_str);

            rpc::Message answer{};
            answer.source = object;
            answer.destination = source_port;
            answer.type = rpc::MessageType::Result;
            answer.session_id = message.session_id;
            answer.opcode = message.opcode;
            answer.payload = s.get_payload();

            chan.send_message(source_port, answer);
        }
        else if (message.opcode == SHUTDOWN_COMMAND)
        {
            fmt::print("Received SHUTDOWN from {}, {}\n", source_port, message.destination);
            chan.stop();
        }
    }
};

int main(int argc, char **argv)
{
    if (argc != 3)
        return -1;
    Handle port_handle = Handle::from_desc(argv[1]);
    int port_id = std::stoi(argv[2]);

    auto port = ipc::Port::from_handle(port_handle);
    rpc::Channel channel(port_id, std::move(port));

    channel.bind_static<SimpleSendReceiver>(0);

    channel.loop();
    return 0;
}
