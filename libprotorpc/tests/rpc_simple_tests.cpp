#include <protoipc/port.hh>
#include <protoipc/router.hh>
#include <protorpc/channel.hh>
#include <protorpc/serializer.hh>
#include <protorpc/unserializer.hh>

#include <fmt/core.h>
#include <gtest/gtest.h>

#include <thread>

constexpr std::uint64_t PING_COMMAND = 42;

class SimpleSendProxy : public rpc::RpcProxy
{
  public:
    SimpleSendProxy(rpc::Channel *chan,
                    rpc::ObjectId object_id,
                    rpc::PortId remote_port,
                    rpc::ObjectId remote_id)
        : rpc::RpcProxy(chan, object_id, remote_port, remote_id)
    {}

    bool ping(std::string ping_str, std::string *output)
    {
        rpc::Message message;
        message.source = id();
        message.destination = remote_id();
        message.opcode = PING_COMMAND;

        rpc::Serializer s;
        s.serialize(ping_str);

        message.payload = s.get_payload();

        rpc::Message result;

        if (!channel_->send_request(remote_port(), message, result))
        {
            fmt::print("[PROXY] There was an error when sending the ping request\n");
            return false;
        }

        rpc::Unserializer u(std::move(result.payload));

        if (!u.unserialize(output))
            throw std::runtime_error("There was an error parsing the ping reply");

        return true;
    }
};

class SimpleSendReceiver : public rpc::RpcReceiver
{
  public:
    void on_message(rpc::Channel &chan,
                    rpc::ObjectId object,
                    rpc::PortId source_port,
                    rpc::Message &message) override
    {
        if (message.opcode == PING_COMMAND)
        {
            fmt::print("Received PING from {},{}\n", source_port, message.destination);

            rpc::Unserializer u(std::move(message.payload));
            std::string ping_str;

            if (!u.unserialize(&ping_str))
                throw std::runtime_error("Could not unserialize ping string");

            rpc::Serializer s;
            s.serialize(ping_str);

            rpc::Message answer{};
            answer.source = object;
            answer.destination = source_port;
            answer.type = rpc::MessageType::Result;
            answer.session_id = message.session_id;
            answer.opcode = message.opcode;
            answer.payload = s.get_payload();

            chan.send_message(source_port, answer);
        }
    }
};

TEST(rpc_test, simple_send)
{
    // Setting up the router between clients
    auto router = ipc::Router::make();

    auto [client_a_id, client_router_a_port] = router->create_port();
    auto [client_b_id, client_router_b_port] = router->create_port();

    // Setting up channels
    rpc::Channel first_channel(client_a_id, std::move(client_router_a_port));
    rpc::Channel second_channel(client_b_id, std::move(client_router_b_port));

    std::thread router_thread([&]() { router->loop(); });

    std::thread emitter_thread([&]() { first_channel.loop(); });

    std::thread receiver_thread([&]() { second_channel.loop(); });

    auto receiver_id = second_channel.bind<SimpleSendReceiver>();
    auto proxy = first_channel.connect<SimpleSendProxy>(client_b_id, receiver_id);

    std::string ping_string = "7253c09bd391db2cd370455fc64e520ac79fca31";
    std::string pong_string;

    bool result = proxy->ping(ping_string, &pong_string);

    ASSERT_EQ(result, true);
    ASSERT_EQ(pong_string, ping_string);

    second_channel.stop();
    first_channel.stop();
    router->terminate();

    router_thread.join();
    receiver_thread.join();
    emitter_thread.join();
}

TEST(rpc_test, simple_multi_proxy)
{
    // Setting up the router between clients
    auto router = ipc::Router::make();

    auto [client_a_id, client_router_a_port] = router->create_port();
    auto [client_b_id, client_router_b_port] = router->create_port();

    // Setting up channels
    rpc::Channel first_channel(client_a_id, std::move(client_router_a_port));
    rpc::Channel second_channel(client_b_id, std::move(client_router_b_port));

    auto receiver_id = second_channel.bind<SimpleSendReceiver>();

    auto first_proxy = first_channel.connect<SimpleSendProxy>(client_b_id, receiver_id);
    auto second_proxy = first_channel.connect<SimpleSendProxy>(client_b_id, receiver_id);

    ASSERT_NE(first_proxy->id(), second_proxy->id());

    std::thread router_thread([&]() { router->loop(); });

    std::thread emitter_thread([&]() { first_channel.loop(); });

    std::thread receiver_thread([&]() { second_channel.loop(); });

    // First proxy
    std::string ping_string = "7253c09bd391db2cd370455fc64e520ac79fca31";
    std::string pong_string;

    bool result = first_proxy->ping(ping_string, &pong_string);

    ASSERT_EQ(result, true);
    ASSERT_EQ(pong_string, ping_string);

    // Second proxy
    ping_string = "8642f7ca4cbc8318b82d44b07e593a49fd35c4bc";
    pong_string = "";

    result = second_proxy->ping(ping_string, &pong_string);

    ASSERT_EQ(result, true);
    ASSERT_EQ(pong_string, ping_string);

    first_channel.stop();
    second_channel.stop();
    router->terminate();

    router_thread.join();
    emitter_thread.join();
    receiver_thread.join();
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
