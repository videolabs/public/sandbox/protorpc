#include <protorpc/channel.hh>
#include <protorpc/serializer.hh>
#include <protorpc/unserializer.hh>

#include <future>
#include <stdexcept>

namespace rpc
{

using PendingRpcMessage = Channel::PendingRpcMessage;

Channel::Channel(std::uint64_t port_id, std::unique_ptr<ipc::Port> port)
    : port_id_(port_id)
    , port_(std::move(port))
    , finished_(false)
{
    request_handling_thread_ = std::thread([this] { request_handling_loop_(); });
    has_message = false;
}

ipc::PortError Channel::next_message_(PendingRpcMessage &pending)
{
    ipc::Message msg;
    ipc::PortError err = port_->receive(msg);

    if (err != ipc::PortError::Ok)
        return err;

    pending.source_port = msg.source;

    // Extract the rpc payload from the message
    rpc::Message result;
    result.handles = std::move(msg.handles);

    Unserializer u(std::move(msg.payload));

    bool status = true;
    std::vector<std::uint8_t> payload;

    status &= u.unserialize(&result.source);
    status &= u.unserialize(&result.destination);
    status &= u.unserialize(&result.type);
    status &= u.unserialize(&result.session_id);
    status &= u.unserialize(&result.opcode);
    status &= u.unserialize(&result.payload);

    if (!status)
        throw std::runtime_error("Could not decode rpc message header");

    err = port_->acknowledge(msg);
    if (err != ipc::PortError::Ok)
        return err;

    // We patch the rpc::Message to indicate the source object.
    pending.destination_object = result.destination;
    result.destination = result.source;
    result.source = pending.destination_object;

    pending.message = std::move(result);
    return ipc::PortError::Ok;
}

void Channel::request_handling_loop_()
{
    while (!finished_)
    {
        PendingRpcMessage curr;
        std::shared_ptr<RpcReceiver> handler;
        {
            std::unique_lock<std::mutex> lk(lock_);
            request_cv_.wait(lk, [this] { return !request_queue_.empty() || finished_; });

            if (finished_)
                break;
            curr = std::move(request_queue_.front());
            request_queue_.pop_front();

            auto found = receivers_.find(curr.destination_object);

            if (found == receivers_.end())
                throw std::runtime_error("Destination object not found");
            handler = found->second;
        }
        handler->on_message(*this, curr.destination_object, curr.source_port, curr.message);
    }
}

ipc::PortError Channel::loop()
{
    while (!finished_)
    {
        PendingRpcMessage msg;
        ipc::PortError err;
        if ((err = next_message_(msg)) != ipc::PortError::Ok)
            return err;
        if (msg.message.type == MessageType::Result)
        {
            std::unique_lock<std::mutex> lk(lock_);
            this->message_cv.wait(lk, [this] { return !this->has_message; });
            this->has_message = true;
            current_pending_result_ = std::move(msg.message);
            result_cv_.notify_all();
        }
        else if (msg.message.type == MessageType::Request)
        {
            std::lock_guard<std::mutex> lk(lock_);
            request_queue_.push_back(std::move(msg));
            request_cv_.notify_all();
        }
        else
            throw std::runtime_error("Message of unknown type received");
    }
    return ipc::PortError::Ok;
}

bool Channel::send_message(std::uint64_t remote_port, rpc::Message &msg)
{
    ipc::Message ipc_msg;

    // This is the ipc layer, destination is remote process id
    ipc_msg.source = port_id_;
    ipc_msg.destination = remote_port;
    ipc_msg.handles = std::move(msg.handles);

    // Encoding the rpc::Message data
    rpc::Serializer s;
    s.serialize(msg.source);
    s.serialize(msg.destination);
    s.serialize(msg.type);
    s.serialize(msg.session_id);
    s.serialize(msg.opcode);

    // payload_size is encoded as part of the vector
    s.serialize(msg.payload);

    ipc_msg.payload = s.get_payload();

    ipc::PortError error = port_->send(ipc_msg);

    // TODO: Return a more explicit error than just "failed"
    return error == ipc::PortError::Ok;
}

bool Channel::send_request(std::uint64_t remote_port, rpc::Message &msg, rpc::Message &result)
{
    msg.type = MessageType::Request;
    msg.nextid();

    if (!send_message(remote_port, msg))
        return false;
    std::unique_lock<std::mutex> lk(lock_);
    result_cv_.wait(lk, [this, msg] {
        return finished_ ||
               (this->has_message && current_pending_result_.session_id == msg.session_id);
    });

    result = std::move(current_pending_result_);
    this->has_message = false;
    this->message_cv.notify_one();

    if (finished_)
        return false;
    return true;
}

void Channel::next_id_()
{
    while (allocated_objects_.find(current_id_) != allocated_objects_.end())
        current_id_++;
}

} // namespace rpc
